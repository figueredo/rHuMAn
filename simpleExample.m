%% Simple Working Example : rHuMAn Library
%%% 
%% Constructing human model from rHuManModel
% 
% First, we construct a human model using rHuManModel with height
% (shoulder!) of 1.35 m. Enabling verbose for illustration.
rhuman = rHuManModel('shoulderHeight',1.35,'verbose',true);

%%
% To visualize human model, display its properties. 
% Note that 'kine' depicts the human kinematics, and 'kineconfig' the general kinematics and 
% geometric configurations (e.g., bounding box defining human body and ball defining human 
% head that are used for fast self-collision analysis, and functions as checkJointLim to 
% verify if a prescribed joint is over the prescrived limits)
rhuman
%%
% To have a quick visualization method to human upper-limb (now, only avaiable for right-handed) 
% You can define a joint configuration.
% 
% The following is one configuration with shoulder elevated 30o at 90o angle (facing in front of the person) 
% with 40o of elbow flexion. You can also print in the screen its position and orientation (quaternion) 
theta = [90; 30; 0; 40;   0; 0; 0]*pi/180; 
rhuman.plot(theta);
view(-70,22);
axis([-0.6 0.6 -0.3 0.6 0.0 1.8]);

position = rhuman.getPos(theta)
orientation = rhuman.getOrientation(theta)
%% Constructing human Manipulability Object from rHuManManipulability
% 
% We take the human model and the existing database located in ./OpenSimData folder 
rhmanip = rHuManManipulability(rhuman,'localdata','./OpenSimData','verbose', true);


%% Building the augmented datastruct 
% 
% Lets build  the augmented datastruct with a set of forces (default 58)
% Input is simply the number of joints. But, to see more options check the
% help for build_AugmentedComfortDataset
datastruct = rhmanip.build_AugmentedComfortDataset(10000); 


%% Building a General Comfortability Distribution
% 
% Now we reshape the datastruct to have a comfortability distribution. 
% 
genManip = rhmanip.reshape_Comfortability('external',datastruct);

% Now plot the comfortability to observe the best configurations considering all possible forces (in all directions) 
% and all accelerations. That is, the distribution of comfortability (assuming the worst possible task) along human workspace
rhmanip.plot_comfortability(genManip,'section',true)  


%% Building a Task-Specific Comfortability Distribution
% 
% Now we reshape the datastruct to have a task-specific comfortability distribution. 
% Let's assume that a given task is defined by the following wrench oracceleration
force = [0; 5; 0;  10; 10; 0]; 
% In other words, a wrench with a torque in y axis and a diagonal force
% pointing 45o in between X and Y axis (that is, in front and outwards the person) 
% 
% Now, let's compute the gain between muscular-informed assessmetn and ergonomics. 
task.speed     = 0.5;  % Medium speed
task.Intensity = 0.9;  % Highly intensive activity (see the forces) 
task.repeat    = 0;    % Let' s assume that this is a 1 time task. 
kMuscle = rhmanip.compute_gainMuscle(task.speed, task.Intensity, task.repeat)

% kMuscle must always return a value between [1]: only muscle to [0] only ergonomics
% 
% Now we reshape the augmented datastruct to have a task-specific comfortability distribution. 
tsManip = rhmanip.reshape_TSComfortability(force, kMuscle, 'external',datastruct);

% Now plot the comfortability to observe the best configurations considering the specific force
% That is, the distribution along workspace where the human can better apply such force/acceleration 
rhmanip.plot_comfortability(tsManip) 

