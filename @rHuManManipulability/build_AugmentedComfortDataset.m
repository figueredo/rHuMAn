%% Build a Comfortability Dataset with Muscular-Informed Manip and Ergonomics  
% 
%--------------------------------------------------------------------------
%  This method is part of the class rHuManManipulability
%  -
%  It builds the comfortability general dataset. 
%  See referenced paper for further details
%-------------------------------------------------------------------------- 
% 
%######################################################################### 
%------------------------[ Outputs ]
%  * datastruct:  returns the a datastruct with joints, pos, rot, ergonomics, muscular-informed manip, and penalties for human workspace. 
%                 Method also updates variables within the object (thus, no need to safe output if it is not going to be used)  
%
% 
%------------------------[ INPUTs: ]  
%  * datasize (double) : defines size of the datastruct (minimum is 100)  
% 
%------------------------[ Optional: INPUTs ]  
% %  * 'default'   : When adding the string 'default', forces will be set for default mode in (58 forces)
% 
%------------------------[ Optional: INPUTs ]:[Format: String followed by values ]
% 
%  * 'forces',double(3,N)   : defines set of N forces for augmented manipulability assessment (This makes entry 'lenght' void)   
%  * 'torques',double(3,N)  : defines set of N torques for augmented manipulability assessment (This makes entry 'lenght' void)   
%  * 'wrenches',double(6,N) : defines set of N wrenches [torque;force] for augmented manipulability assessment (This makes entries 'lenght','forces','torques' void) 
% 
%  * 'wrist',logical            : If forces/torques are defined in the end-effector (not in DEFAULT: task-space). Default=false.  
%  * 'wristframe',logical       : same as above. Default=false.
%  * 'end',logical              : same as above. Default=false.
%  * 'endeffector',logical      : same as above. Default=false.
% 
% 
%------------------------[ EXAMPLES ]
% build_AugmentedComfortDataset(600000);
% build_AugmentedComfortDataset(300000,'forces',randn(3,30),'torques',randn(3,10));
% build_AugmentedComfortDataset(600000,'wrenches',randn(6,30));
% build_AugmentedComfortDataset(1200000,'forces',randn(3,6),'wrist');
% 
%######################################################################### 


%% Function fct_get_muscularCost
function database = build_AugmentedComfortDataset(obj, datasize, defaulttext, options) 
    %==============[ Check number of extra inputs ]
    arguments
        obj
        % Extra arguments [ check frame of assessment ] 
        datasize            double;        % defines size of the datastruct (minimum is 100)  
        defaulttext         char = '';
%         options.forces   options.forces {mustBeOfSize(options.forces,3)}     % Double(3,N) - Defines set of N forces for augmented manipulability assessment (This makes entry 'lenght' void)   
%         options.torques  options.torques {mustBeOfSize(options.torques,3)}   % Double(3,N) - Defines set of N torques for augmented manipulability assessment (This makes entry 'lenght' void)   
%         options.wrenches options.wrenches {mustBeOfSize(options.wrenches,6)} % Double(6,N) - Defines set of N wrenches [torque;force] for augmented manipulability assessment (This makes entries 'lenght','forces','torques' void)         
%         
        options.forces      double    % Optional: Double(3,N) - Defines set of N forces for augmented manipulability assessment (This makes entry 'lenght' void)   
        options.torques     double    % Optional: Double(3,N) - Defines set of N torques for augmented manipulability assessment (This makes entry 'lenght' void)   
        options.wrenches    double    % Optional: Double(6,N) - Defines set of N wrenches [torque;force] for augmented manipulability assessment (This makes entries 'lenght','forces','torques' void) 
        %
        options.wrist       logical = false;  % [Optional] If forces are defined in the end-effector (not in DEFAULT: task-space). Default=false.  
        options.wristframe  logical = false;  % [Optional] If forces are defined in the end-effector (not in DEFAULT: task-space). Default=false.  
        options.end         logical = false;  % [Optional] If forces are defined in the end-effector (not in DEFAULT: task-space). Default=false.  
        options.endeffector logical = false;  % [Optional] If forces are defined in the end-effector (not in DEFAULT: task-space). Default=false.         
    end
    
%% Check number minimum of entries: 100
    if datasize <100
        error([10,9,'*** Number minimum for dataset lenght is 100. For values below use function getComfortValue'])
    end                

%% Check if force is defined over wrist or task-space(default)    
    if options.wrist || options.wristframe || options.end || options.endeffector 
        obj.FLAG_wristFrame = true;
    else
        obj.FLAG_wristFrame = false;
    end        
    
%% If forces are being defined from input

    if strcmpi(defaulttext,'default')
        % Default forces
        obj.setForces4Manipulability();
        obj.printv(obj,'*** Defining force for manipulability assessment as default.')
    else
        % Manually input
        if isfield(options,'wrenches')
            obj.printv(obj,'*** Adjusting forces for manipulability assessment with wrenches values.')
            obj.setForces4Manipulability('wrenches',options.wrenches);
            

        elseif isfield(options,'forces')  && ~isfield(options,'torques')
            obj.printv(obj,'*** Adjusting forces for manipulability assessment with force values.')
            obj.setForces4Manipulability('forces',options.forces);

        elseif isfield(options,'torques') && ~isfield(options,'forces')
            obj.printv(obj,'*** Adjusting forces for manipulability assessment with torque values.')
            obj.setForces4Manipulability( 'torques',options.torques);

        elseif isfield(options,'forces')  && isfield(options,'torques')                
            obj.printv(obj,'*** Adjusting forces for manipulability assessment with force and torque values.')
            obj.setForces4Manipulability('forces',options.forces,'torques',options.torques);
        end
    end
    
%%  Building data-struct

% Number of forces to analyze
numForces = size(obj.configForces,2);

% Temporary Struct (not to lose the previous one in case of failure)
tempObj.datasetSize = datasize;
tempObj.joints      = zeros(7,datasize);
tempObj.pos         = zeros(3,datasize);
tempObj.rot         = zeros(4,datasize);
tempObj.ergoManip   = zeros(1,datasize);
tempObj.muscInfoManip   = zeros(numForces,datasize);
tempObj.penalty_selfCol = zeros(numForces,datasize);

forcevector = obj.configForces; 
if isempty( obj.indJacobSVDvec )
    FLAG_ELLIPS = false;
else
    FLAG_ELLIPS = true;
    forcevector_indSVDtorque = intersect( obj.indJacobSVDvec, obj.indTorques);   
    forcevector_indSVDforce  = intersect( obj.indJacobSVDvec, obj.indForces);
end
    
    

% Configuration values
it = 1;
joints = obj.rhuman.getRandJoints('length',datasize);
FLAG_INVALID_JOINT = false;

verboseOld = obj.verbose;
obj.verbose = false;
ticstart = tic;
while(it <= datasize)    
    

    % Get random configuration
    %---------------------------------------   
    if FLAG_INVALID_JOINT
        joints(:,it) = obj.rhuman.getRandJoints();
    end
    FLAG_INVALID_JOINT = true;
    
    %---------------------------------------
    
%     obj.configForces
	if FLAG_ELLIPS
        J = ( obj.rhuman.getJacobGeom(joints(:,it)) );
        [U1,~,~] = svd( J(1:3,:) );
        [U2,~,~] = svd( J(4:6,:) );    
        forcevector(1:3, forcevector_indSVDtorque )  = U1;
        forcevector(4:6, forcevector_indSVDforce  )  = U2;
    end
    
    % Get muscular activity values   
    [~,MuscleTransRate_vec,~, penalties_vec ] = obj.get_muscularCost( joints(:,it), forcevector, 'wrist', obj.FLAG_wristFrame );

    % Collision check
    if min(penalties_vec)==0
        continue;
    end
       
    xm  = obj.rhuman.getFKM(joints(:,it));
    pos = xm.translation.q(2:4);
    rot = xm.P.q(1:4);
    % Get ergonomic values (RULA)    
    RULA        = obj.get_RULA( joints(:,it) );
    RULA_value  = obj.handleRula2ErgoIndex(RULA);

                
    % Store data and go to next! 
    %---------------------------------------   
    FLAG_INVALID_JOINT = false;    
    tempObj.pos(:,it) = pos;
    tempObj.rot(:,it) = rot;
%     %----------
    tempObj.ergoManip(it)       = RULA_value;
    tempObj.muscInfoManip(:,it)   = MuscleTransRate_vec;
    tempObj.penalty_selfCol(:,it) = penalties_vec;
    % Update index
    it = it + 1;       
    
    percentageCompleted = it/datasize;
    if mod(percentageCompleted*100,5)==0
        disp(['*** Percentage completed: ',num2str(percentageCompleted*100),'% at ',num2str(toc(ticstart)),' sec'])
    end

end
obj.verbose = verboseOld;



%% Storing configuration 
obj.configDataset.forces     = obj.configForces;
obj.configDataset.indForces  = obj.indForces;
obj.configDataset.indTorques = obj.indTorques;
obj.configDataset.indJacobSVDvec = obj.indJacobSVDvec;
obj.configDataset.torqueNormalization = obj.torqueNormalization;
obj.configDataset.FLAG_wristFrame     = obj.FLAG_wristFrame;  


%% Storing data

obj.datasetSize = tempObj.datasetSize;
obj.joints      = joints;
obj.pos         = tempObj.pos;
obj.rot         = tempObj.rot;
obj.ergoManip   = tempObj.ergoManip;
% obj.muscInfoManip   = setComfResult;
obj.muscInfoManip   = tempObj.muscInfoManip;
obj.penalty_selfCol = tempObj.penalty_selfCol;
obj.comfortIndex  = [];
clear tempObj


database.size   = obj.datasetSize;
database.joints = joints;
database.pos = obj.pos;
database.rot = obj.rot;
database.ergoManip = obj.ergoManip;
database.muscInfoManip = obj.muscInfoManip;
database.penalty_selfCol = obj.penalty_selfCol;
database.configDataset      = obj.configDataset;
database.rhumanModel        = obj.rhuman;


end
