%% Plots a human model with the comfortability distribution 
% 
% %--------------------------------------------------------------------------
%  This method is part of the class rHuManManipulability
%  -
%  This method plots a human model and scatter plot showing comfort values.
% 
%--------------------------------------------------------------------------
% 
%#########################################################################
% 
%------------------------[ INPUT ]
% 
%  * 'external',ComfDataDist  :  Explores an external comfortability distribution. Different from dataset, the structu must have a 'comfortIndex'   
%                                External datastruct must contain: {pos, comfortIndex}   
% 
%------------------------[ Optional: INPUTs ]
% [Format: String followed by values ]
% 
%  * 'animation',logical       :  Defines if animation is on/off
%  * 'animationDelay',double   :  Animation time (between pauses). If ('animationDelay',0) then, it pauses until user press a key [Default: 0.1].
% 
%  * 'savefig',char            :  If saving figure, please specifiy the address 
% 
%  * 'section',logical      : If plot will be presented in transversal sections or full (false=default).
%  * 'section_num',double   : Number os sections (default=5).
%  * 'section_axis',double  : Defines axis for section  x=1;  y=2;  z=3;  (default=3).
% 
%  * 'noModel',logical      : False; % Set this to true if you do not wish to plot human model 
% 
%------------------------[ Example ]
%  plot_comfortability(obj, comfDataDist,options  ) 
%  plot_comfortability(obj, comfDataDist,options  ) 
%  plot_comfortability(obj, comfDataDist,options  ) 
% 
%######################################################################### 


%% Plot Comfortability (Plot distribution)
function plot_comfortability(obj, comfDataDist,  options  ) 
    %==============[ Check number of extra inputs ]
    arguments        
        % Fixed Argument
        obj
        comfDataDist        
        % Extra arguments [ check frame of assessment ] 
        
        options.animation   logical = false;     % Defines if animation is on/off
        options.animationDelay double = 0.1;     % Animation time (between pauses). If ('animationDelay',0) then, it pauses until user press a key.

        options.savefig     char;                % If empty, ignore. Defines which file should be saved a figure (if left empty, no fig is saved).
        
        options.section         logical = false; % If plot will be presented in transversal sections or full (false=default).
        options.section_num     double =  5; % Number of sections
        options.section_axis    double  = 3; % Axis:  x=1;  y=2;  z=3;
        
        options.noModel         logical = false; % Set this to true if you do not wish to plot human model 
    end
    
    
%% Check if datastruct is external and valid entries

    % Check if using external datastruct
    if ~isfield(comfDataDist,'pos'),  error([10,'*** comfDataDist must have a field ''pos'' with position data [x;y;z] for all distribution entries (double(3,N))',10]); end
    if ~isfield(comfDataDist,'comfortIndex'),  error([10,'*** comfDataDist must have a field ''comfortIndex'' with all comfortability distribution entries (double(1,N))',10]); end
    distanceCart = obj.voxConfig.distanceCart;
    if isfield(comfDataDist,'voxConfig')
        if isfield(comfDataDist.voxConfig,'distanceCart')
            distanceCart = comfDataDist.voxConfig.distanceCart;
        end
    end
        
   
%%

if ~options.noModel

    clear robot
    robot = importrobot('humanmodel.urdf');
    % tform1 = trvec2tform([-0.1809, +0.009176, -0.4498]);
    tform1 = trvec2tform([-0.1809, +0.009176, -0.3498]);
    tform = tform1*quat2tform([cos(pi/4), 0, 0, sin(pi/4)]);
    originbase{1} = robot.Bodies{1}.Joint.JointToParentTransform;
    originbase{2} = robot.Bodies{28}.Joint.JointToParentTransform;
    originbase{3} = robot.Bodies{36}.Joint.JointToParentTransform;
    setFixedTransform(robot.Bodies{1}.Joint,  tform*originbase{1});
    setFixedTransform(robot.Bodies{28}.Joint, tform*originbase{2});
    setFixedTransform(robot.Bodies{36}.Joint, tform*originbase{3});
    figure, 
    show(robot)  

    hold on;
    grid on;  

else
    figure, hold on; grid on;  
end

%-------------------
X = comfDataDist.pos(1,:);
Y = comfDataDist.pos(2,:);
Z = comfDataDist.pos(3,:) - obj.rhuman.kine.base.translation.q(4);

figx = scatter3(X, Y,Z, 10, [comfDataDist.comfortIndex], 'filled'); 

colormap jet
% colormap(flipud(hot))
% colormap(flipud(summer))
% colormap(flipud(bone))
% axis([-1 0.75 -0.75 0.75  -1.5 0.75 ])
axis([-1 0.75 -0.75 0.75  -0.6 0.75 ])

%%
% viewdata = [60,5  ;60 30;  75 25;  60 45;  60 60;   85 12;  100 15];

maxrange = obj.rhuman.kineconfig.totalRange;

if options.section_axis==3
    sect_axis = Z;
elseif options.section_axis==2
    sect_axis = Y;
elseif options.section_axis==1
    sect_axis = X;
else
    disp('*** Warning: section_axis should be selected between 1 and 3. Automatically setting to 3 (Z).' )
    sect_axis = Z;
end


%% Plot animation
if options.animation
  
    irange(1,:) = .9*maxrange:-min(distanceCart*1.1,((1.9*maxrange)/20)):-maxrange;
    irange(2,:) = irange(1,:)+distanceCart;
    delete(figx),
    hold on,

    disp('press to start animation')
    pause
    
    
    
    for i=1:size(irange,2)
        indexZzeroLogical = (  logical( logical( sect_axis>=irange(1,i) ).*logical(sect_axis<irange(2,i)) ));
        %figx = scatter3(X(indexZzeroLogical), Y(indexZzeroLogical),Z(indexZzeroLogical), 50, comfDataDist.comfortIndex(indexZzeroLogical), 'filled','Marker','s');
        figx = scatter3(X(indexZzeroLogical), Y(indexZzeroLogical),Z(indexZzeroLogical), 10, comfDataDist.comfortIndex(indexZzeroLogical), 'filled');
        if options.animationDelay==0
            pause
        else
           pause(options.animationDelay) 
        end
    end  

    
end
%% Output Section
if options.section

    irange(1,:) = .9*maxrange:-((1.9*maxrange)/(options.section_num-1)):-maxrange;
    irange(2,:) = irange(1,:)+distanceCart;

    % irange(1,:) = .50:-(1.0*2/8):-.50;
    % irange(2,:) = irange(1,:)+0.025;
    delete(figx),
    hold on,
    for i=1:size(irange,2)
        indexZzeroLogical = (  logical( logical( sect_axis>=irange(1,i) ).*logical(sect_axis<irange(2,i)) ));
        figx = scatter3(X(indexZzeroLogical), Y(indexZzeroLogical),Z(indexZzeroLogical), 50, comfDataDist.comfortIndex(indexZzeroLogical), 'filled','Marker','s');
    end     

end


    
end


%         options.animation   logical = false;     % Defines if animation is on/off
%         options.animationDelay double = 0.1;     % Animation time (between pauses). If ('animationDelay',0) then, it pauses until user press a key.
% 
%         options.savefig     char;                % If empty, ignore. Defines which file should be saved a figure (if left empty, no fig is saved).
%         
%         options.section         logical = false; % If plot will be presented in transversal sections or full (false=default).
%         options.section_num     double =  5; % Number of sections
%         options.section_axis    double  = 3; % Axis:  x=1;  y=2;  z=3;
