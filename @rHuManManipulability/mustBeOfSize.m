%% Custom validator functions
function mustBeOfSize(input,rowSize)
    if ~isa(input,'double')
        error([10, strcat('*** Input must be of class: double')])
    end
    if size(input,1) ~= rowSize
        error([10, strcat('*** Input must be of size(rows): ',9,num2str(rowSize))])
    end
end   