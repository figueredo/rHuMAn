%% Reshape an Augmented Comfortability Dataset into a comfortability distribution 
% 
%--------------------------------------------------------------------------
%  This method is part of the class rHuManManipulability
%  -
%  It builds the comfortability general manipulability from an existing
%  augmented data-struct with muscular and ergonomics data (those are not
%  to be updated).
%  See referenced paper for further details
%-------------------------------------------------------------------------- 
% 
%######################################################################### 
%------------------------[ Outputs ]
%  * datastruct:  returns the comfortability distribution datastruct with joints, pos, rot, ergonomics, muscular-informed manip, and penalties for human workspace. 
% 
%------------------------[ INPUT ]
% 
%  * kMuscle (double) : Gain between [0,1] to weight muscular (1) and ergonomics (0)
%                       Example 0.5 (equal weights), while 1 implies only
%                       muscular-assessment [Default = 0.5 ]
% 
% 
%------------------------[ Optional: INPUTs ]:[Format: String followed by values ]
% 
%  * 'external',datastruct     : Explores an external datastruct instead of the one in the object itself. [default] 
%                                External datastruct must contain: {datasetSize,joints,pos,rot,ergoManip,muscInfoManip,penalty_selfCol}   
%  
%              
% 
%------------------------[ EXAMPLES ]
% comfDistribution = reshape_Comfortability(0.5);                           % Returns a comfortability distribution datastruct from data in the obj (expects prior use of build_AugmentedComfortDataset())   
% comfDistribution = reshape_Comfortability(0.75, 'external',extDataset);   % Returns a comfortability distribution datastruct from extDataset   
% 
% 
%######################################################################### 


%% Function reshape_Comfortability         
function comfDist = reshape_Comfortability(obj, kMuscle, options) 
    %==============[ Check number of extra inputs ]
    arguments
        obj
        % Extra arguments [ check frame of assessment ] 
        kMuscle             double=0.5;     % Gain between [0,1] to weight muscular (1) and ergonomics (0) 
        options.external                    % [Optional] If forces are defined in the end-effector (not in DEFAULT: task-space). Default=false.         
    end
    
    
    
%% Check if datastruct is external and valid entries
    
    database.size   = obj.datasetSize;
    database.joints = obj.joints;
    database.pos    = obj.pos;
    database.rot    = obj.rot;
    database.ergoManip          = obj.ergoManip;
    database.muscInfoManip      = obj.muscInfoManip;
    database.penalty_selfCol    = obj.penalty_selfCol;
    
    database.configDataset      = obj.configDataset;
    FLAG_NO_CONFIG = false;

              
    % Check if using external datastruct
    if isfield(options,'external')
        database = options.external;
        if ~isfield(database,'size'), error([10,'*** reshape_Comfortability(''external'',var), where var is the datastruct. Datastruct must contain entry:  size',10]); end
        if ~isfield(database,'joints'), error([10,'*** reshape_Comfortability(''external'',var), where var is the datastruct. Datastruct must contain entry:  joints',10]); end
        if ~isfield(database,'pos'), error([10,'*** reshape_Comfortability(''external'',var), where var is the datastruct. Datastruct must contain entry:  pos',10]); end
        if ~isfield(database,'rot'), error([10,'*** reshape_Comfortability(''external'',var), where var is the datastruct. Datastruct must contain entry:  rot',10]); end
        if ~isfield(database,'ergoManip'), error([10,'*** reshape_Comfortability(''external'',var), where var is the datastruct. Datastruct must contain entry:  ergoManip',10]); end
        if ~isfield(database,'muscInfoManip'), error([10,'*** reshape_Comfortability(''external'',var), where var is the datastruct. Datastruct must contain entry:  muscInfoManip',10]); end
        if ~isfield(database,'penalty_selfCol'), error([10,'*** reshape_Comfortability(''external'',var), where var is the datastruct. Datastruct must contain entry:  penalty_selfCol',10]); end                
        obj.printv(obj,'*** Using external augmented datastruct for shaping a comfortability distribution analysis.')        
        
        if ~isfield(database,'configDataset')
            disp([10,'*** Warning: External augmented datastruct does not contains struct ''configDataset''. Results might be influenced. ',10]); 
            FLAG_NO_CONFIG = true;
        else
            if ~isfield(database.configDataset,'indForces') || ~isfield(database.configDataset,'indTorques')                
                FLAG_NO_CONFIG = true;
            end
            
        end   
  
        
    end

    % Validate entries (all entries must have the same size from size( database.joints,2) 
    if database.size <= 0, error([10,'*** Datastruct size is below or equal to 0. No data for analysis',10]); end
    if size( database.joints,2) ~= database.size
        if size( database.joints,2)==7 && size( database.joints,1)==database.size
            database.joints = database.joints';
        else
            database.size = database.joints;
            obj.printv(obj,'*** Warning: Joints in datastruct has different size from database.size (which has been updated for this analysis).')        
        end
    end
    if size( database.pos,2) ~= database.size; error([10,'*** Datastruct pos has a different size from database.size',10]); end
    if size( database.rot,2) ~= database.size; error([10,'*** Datastruct rot has a different size from database.size',10]); end
    if size( database.ergoManip,2) ~= database.size; error([10,'*** Datastruct ergoManip has a different size from database.size',10]); end
    if size( database.muscInfoManip,2) ~= database.size; error([10,'*** Datastruct muscInfoManip has a different size from database.size',10]); end
    if size( database.penalty_selfCol,2) ~= database.size; error([10,'*** Datastruct penalty_selfCol has a different size from database.size',10]); end
    
   

%% Normalizing Muscular-Informed Manipulability (or Transmission-Rate) with best solution in workspace
 
% For each force-vector, find the minimum muscular-informed manipulability
% index (see referenced paper for further details) 
% --> Transform (F,N) matrix into a (1,N), where F is the number of forces and N is the input data-size 
%
if ~FLAG_NO_CONFIG 
    % Get torque maximum muscular influence equal to force.
    tmp_MiM_force  = min ( database.muscInfoManip(database.configDataset.indForces) );    
    tmp_MiM_torque = min ( database.muscInfoManip(database.configDataset.indTorques) );
    
    % Get the best configuration in the whole workspace (according to the explored N-sized datastruct)  
    obj.voxConfig.maxMiM = max( rmoutliers( tmp_MiM_force ) );
    %max( rmoutliers( MiMindexes, 'percentiles',[5,95] ) )      
    
    % Normalize torques to have in the best case, the same influence of forces   
    database.muscInfoManip(database.configDataset.indTorques) = ...
        database.muscInfoManip(database.configDataset.indTorques).*( obj.voxConfig.maxMiM/ rmoutliers( tmp_MiM_torque ) );

    % Get the list of minimum muscular-informed manipulability
    MiMinds = min ( database.muscInfoManip );      
else
    % Get the list of minimum muscular-informed manipulability
    MiMinds = min ( database.muscInfoManip );    
    % Get the best configuration in the whole workspace (according to the explored N-sized datastruct)  
    obj.voxConfig.maxMiM = max( rmoutliers( MiMinds ) );
    %max( rmoutliers( MiMindexes, 'percentiles',[5,95] ) )    
end
obj.voxConfig.maxMiTR =0;

% Upper bound for every entry
MiMinds = min( obj.voxConfig.maxMiM, MiMinds); 

% Print information (if verbose)
obj.printv(obj,strcat([10,'*** Maximum muscular-informed manipulability (MiM) index computed in the workspace is: ',num2str(obj.voxConfig.maxMiM),10])  )        

% Normalize muscular informed transmission rate
MiMinds = MiMinds./obj.voxConfig.maxMiM;



%% Shaping comfortability distribution
% Distance in voxalized data
diststep = obj.voxConfig.distanceCart;
diststep = sqrt(1*diststep^2);

% Get workspace
tmpxyz=[database.pos(1,:)];  rangeXYZ{1} = min(tmpxyz) :diststep: max(tmpxyz);  % Range for X
tmpxyz=[database.pos(2,:)];  rangeXYZ{2} = min(tmpxyz) :diststep: max(tmpxyz);  % Range for Y
tmpxyz=[database.pos(3,:)];  rangeXYZ{3} = min(tmpxyz) :diststep: max(tmpxyz);  % Range for Z
IAll = database.pos;


% Output Datastruct
comfDist.datasetSize = 0;
comfDist.joints      = [];
comfDist.pos         = [];
comfDist.rot         = [];
comfDist.ergoManip   = [];
comfDist.muscInfoManip   = [];
comfDist.penalty_selfCol = [];


obj.printv(obj,strcat([10,'*** shaping the comfortability distribution... ',10])  )        
% Going over the workspace
databaseindex = 1:1:database.size;    
it = 0; 
ix_ind = 0;
ticstart = tic;
for ix = rangeXYZ{1}
    ix_ind = 1+ix_ind;
    for iy = rangeXYZ{2}
        for iz = rangeXYZ{3}
            
            % Check every inch!
            D = [ix; iy; iz];  
            % Distances to this vox
            distances = pdist2(D', IAll');            
            % Indexes of distances below threshold
            vectorValidDist = distances < diststep;             
            indexes = databaseindex(vectorValidDist);
            
            if ~isempty(indexes)
                it = it+1;
                              
                % Getting the best configuration among the ones that satisfies the voxalized constraint 
                [comfDist.comfortIndex(1,it), indexVox] = ...
                      max( database.penalty_selfCol(vectorValidDist).* ...
                          ( kMuscle*MiMinds(vectorValidDist) ...
                          + (1-kMuscle)*database.ergoManip(vectorValidDist)  )  );
                                     
                comfDist.joints(:,it)      = database.joints(:,indexVox);
                comfDist.pos(:,it)         = D;
                comfDist.rot(:,it)         = database.rot(:,indexVox);
                comfDist.ergoManip(:,it)       = database.ergoManip(indexVox);
                comfDist.muscInfoManip(:,it)   = MiMinds(:,indexVox);
                comfDist.penalty_selfCol(:,it) = database.penalty_selfCol(:,indexVox);                       
            end
            
        end
    end
    if obj.verbose
        percentageCompleted = ix_ind/length(rangeXYZ{1});
        if mod(percentageCompleted*100,10)==0
            disp(['*** Percentage completed: ',num2str(percentageCompleted*100),'% at ',num2str(toc(ticstart)),' sec'])
        end    
    end
end

% Total size:
comfDist.size = it;
clear tempObj





%% Data output

if ~FLAG_NO_CONFIG 
    comfDist.configDataset      = database.configDataset;
end
comfDist.voxConfig          = obj.voxConfig;
comfDist.rhumanModel        = obj.rhuman;
    
    
    
    
  

end
