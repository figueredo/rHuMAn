%% Returns a Task-Specific Comfortability database from an Augmented comfortability dataset 
% 
%--------------------------------------------------------------------------
%  This method is part of the class rHuManManipulability
%  -
%  It builds the Task-Specific comfortability datastruct from an existing
%  augmented data-struct with muscular and ergonomics data (those are not
%  to be updated).
%  See referenced paper for further details
%-------------------------------------------------------------------------- 
% 
%######################################################################### 
%------------------------[ Outputs ]
%  * datastruct:  returns the (TS) Task-specific comfortability datastruct with joints, pos, rot, ergonomics, muscular-informed manip, and penalties for human workspace. 
% 
%------------------------[ INPUT ]
% 
%  * force    (double(6,1) OR double(3,1)) : Defines the task-specific force for analysis. 
%                                            It accepts a Double(6,1) - for a wrench [torque;force] 
%                                            OR Double(3,1) - for a wrench [0;force] with only a force.
% 
% 
%------------------------[ Optional: INPUTs ]:[Format: String followed by values ]
% 
%  * 'external',datastruct     : Explores an external datastruct instead of the one in the object itself. [default] 
%                                External datastruct must contain: {datasetSize,joints,pos,rot,ergoManip,muscInfoManip,penalty_selfCol}   
% 
%  * 'wrist',logical            : If forces/torques are defined in the end-effector (not in DEFAULT: task-space). Default=false.  
%  * 'wristframe',logical       : same as above. Default=false.
%  * 'end',logical              : same as above. Default=false.
%  * 'endeffector',logical      : same as above. Default=false.
%              
% 
%------------------------[ EXAMPLES ]
% tsDatabase = get_TSComfortDataset(randn(3,1));                          % Returns a comfortability datastruct from data in the obj (expects prior use of build_AugmentedComfortDataset())   
% tsDatabase = get_TSComfortDataset(randn(6,1), 'external',extDataset);   % Returns a comfortability datastruct from extDataset   
% tsDatabase = get_TSComfortDataset(force,'wrist',true);                  % Similar assessment but with force defined in end-effector frame
% 
% 
%######################################################################### 



%% Function get_TSComfortDataset         
function tsDatabase = get_TSComfortDataset(obj, force, options) 
    %==============[ Check number of extra inputs ]
    arguments
        obj
        % Extra arguments [ check frame of assessment ] 
        force               double;        % Defines the task-specific force for analysis. It accepts a Double(6,1) - for a wrench [torque;force] OR Double(3,1) - for a wrench [0;force] with only a force.
        options.external                    % [Optional] If forces are defined in the end-effector (not in DEFAULT: task-space). Default=false.         
        %
        options.wrist       logical = false;  % [Optional] If forces are defined in the end-effector (not in DEFAULT: task-space). Default=false.  
        options.wristframe  logical = false;  % [Optional] If forces are defined in the end-effector (not in DEFAULT: task-space). Default=false.  
        options.end         logical = false;  % [Optional] If forces are defined in the end-effector (not in DEFAULT: task-space). Default=false.  
        options.endeffector logical = false;  % [Optional] If forces are defined in the end-effector (not in DEFAULT: task-space). Default=false.         
    end
    

%% Check if force is defined over wrist or task-space(default)    
    if options.wrist || options.wristframe || options.end || options.endeffector 
        FLAG_wristFrame = true;
    else
        FLAG_wristFrame = false;
    end        
    
%% Set task-specific forces 

    % Transpose if column form
    if size(force,2)~=1,  force = force';    end
    
    % Building wrench 
    if size(force,1)==3,  force = [zeros(3,1); force]; end
    
    % Argument validator for wrench
    if length(force)~=6  
        error(['*** get_TSComfortDataset() function takes (datasize, force, [optional:''wrist'']) as input. Force is a wrench and must be defined as a double(6,1) or double(3,1).']);
    end
    
    
    % Getting magnitude of force
    forceMag_torque = norm(force(1:3));
    forceMag_force  = norm(force(4:6));    
    %
    if abs(1-norm(force)) > 10^-6
        obj.printv(obj,['*** Normalizing input force.'])
        forceMag = norm(force);
        force = force/forceMag;
    end

       
    
%% Check if datastruct is external and valid entries
    
    database.size   = obj.datasetSize;
    database.joints = obj.joints;
    database.pos    = obj.pos;
    database.rot    = obj.rot;
    database.ergoManip          = obj.ergoManip;
    database.muscInfoManip      = obj.muscInfoManip;
    database.penalty_selfCol    = obj.penalty_selfCol;
    
    database.configDataset      = obj.configDataset;
    FLAG_NO_CONFIG = false;

              
    % Check if using external datastruct
    if isfield(options,'external')
        database = options.external;
        if ~isfield(database,'size'), error([10,'*** get_TSComfortDataset(''external'',var), where var is the datastruct. Datastruct must contain entry:  size',10]); end
        if ~isfield(database,'joints'), error([10,'*** get_TSComfortDataset(''external'',var), where var is the datastruct. Datastruct must contain entry:  joints',10]); end
        if ~isfield(database,'pos'), error([10,'*** get_TSComfortDataset(''external'',var), where var is the datastruct. Datastruct must contain entry:  pos',10]); end
        if ~isfield(database,'rot'), error([10,'*** get_TSComfortDataset(''external'',var), where var is the datastruct. Datastruct must contain entry:  rot',10]); end
        if ~isfield(database,'ergoManip'), error([10,'*** get_TSComfortDataset(''external'',var), where var is the datastruct. Datastruct must contain entry:  ergoManip',10]); end
        if ~isfield(database,'muscInfoManip'), error([10,'*** get_TSComfortDataset(''external'',var), where var is the datastruct. Datastruct must contain entry:  muscInfoManip',10]); end
        if ~isfield(database,'penalty_selfCol'), error([10,'*** get_TSComfortDataset(''external'',var), where var is the datastruct. Datastruct must contain entry:  penalty_selfCol',10]); end                
        obj.printv(obj,'*** Using external augmented datastruct for shaping a comfortability distribution analysis.')        
        
        if ~isfield(database,'configDataset')
            error([10,'*** External augmented datastruct does not contains struct ''configDataset''. Field required for TScomfortability. ',10,'*** The required field must contain the set of ''configDataset.forces'' (double(num_force,N)) used in building the datastruct. ' ]); 
        else
            if ~isfield(database.configDataset,'forces')
                error([10,'*** External augmented datastruct does not contains field ''forces'' (double(num_force,N)) in configDataset.forces. Field required for TScomfortability. ']); 
            end
            if ~isfield(database.configDataset,'indForces') || ~isfield(database.configDataset,'indTorques')                
                FLAG_NO_CONFIG = true;
            end
        end                
    end
    
    % Validate entries (all entries must have the same size from size( database.joints,2) 
    if database.size <= 0, error([10,'*** Datastruct size is below or equal to 0. No data for analysis',10]); end
    if size( database.joints,2) ~= database.size
        if size( database.joints,2)==7 && size( database.joints,1)==database.size
            database.joints = database.joints';
        else
            database.size = database.joints;
            obj.printv(obj,'*** Warning: Joints in datastruct has different size from database.size (which has been updated for this analysis).')        
        end
    end
    if size( database.pos,2) ~= database.size; error([10,'*** Datastruct pos has a different size from database.size',10]); end
    if size( database.rot,2) ~= database.size; error([10,'*** Datastruct rot has a different size from database.size',10]); end
    if size( database.ergoManip,2) ~= database.size; error([10,'*** Datastruct ergoManip has a different size from database.size',10]); end
    if size( database.muscInfoManip,2) ~= database.size; error([10,'*** Datastruct muscInfoManip has a different size from database.size',10]); end
    if size( database.penalty_selfCol,2) ~= database.size; error([10,'*** Datastruct penalty_selfCol has a different size from database.size',10]); end
    %
    % Number of forces must match the muscInfoManip database
    if size( database.muscInfoManip,1) ~= size( database.configDataset.forces,2); error([10,'*** Datastruct muscInfoManip has a different size from database.configDataset.forces',10]); end
    

%% Adjusting in case of wrist frame task (or database)

% Check if the database has been constructed with wrist relative frame 
if isfield(database.configDataset,'FLAG_wristFrame')
    % If that is the case, then negate the input request 
    if database.configDataset.FLAG_wristFrame
        FLAG_wristFrame = ~FLAG_wristFrame;
    end
end



if FLAG_wristFrame        
    
    forcevector = zeros(6, database.size);    
    % Transforming the task-specific wrist force to task-space forces (or vice versa) 
    for i = 1:database.size
        % if database is constructed in wrist frame (and task in task-space) => map task force to wrist frame    
        if database.configDataset.FLAG_wristFrame
            rot = DQ( database.rot(:,i))';
        else            
        % if database is constructed in task-frame (and task in wrist) => map task force to task-frame                
            rot = DQ( database.rot(:,i));
        end            
        forcevector(1:3,i) = rot*force(1:3)*rot';         
        forcevector(4:6,i) = rot*force(4:6)*rot';         
    end  
    
    
end
    

%% Getting closest wrenches to the provided task-force (if WristFrame is enabled) 
if FLAG_wristFrame  
    
    if ~FLAG_NO_CONFIG     
        % 3 x N  dot product from forcevector (6xN) vector for each F force configuration
        % Results in a Ft x N    and    Ff x N   
        cosAngDist_torques = (database.configDataset.forces(1:3,database.configDataset.indTorques)'*forcevector(1:3,:) );    
        cosAngDist_forces  = (database.configDataset.forces(4:6,database.configDataset.indForces)'*forcevector(4:6,:) );    
        % Obatin the configuration closest to cos(0)=1 where 0 would be the exactly same force.  
        % Results indexes (1,N) for forces closest to prescribed force          
        [~, cosAngIndex_torques] = max(abs(cosAngDist_torques  ));
        [~, cosAngIndex_forces]  = max(abs(cosAngDist_forces   ));
                
        for i=1:database.size   
            % Get muscular activity in these positions (for all N entries)
            
            musculardata_torques(1,i) = database.muscInfoManip( database.configDataset.indTorques(cosAngIndex_torques(i)) ,i);    
            musculardata_forces(1,i)  = database.muscInfoManip( database.configDataset.indForces( cosAngIndex_forces(i) ) ,i);                            
            penaltdata(1,i)  = database.penalty_selfCol( database.configDataset.indForces( cosAngIndex_forces(i) ) ,i);    
        end 
        
    else
        % 3 x N  dot product from forcevector (6xN) vector for each F force configuration
        % Results in a F x N    
        cosAngDist_ = (database.configDataset.forces(:,:)'*forcevector );    
        % Obatin the configuration closest to cos(0)=1 where 0 would be the exactly same force.  
        % Results indexes (1,N) for forces closest to prescribed force          
        [~, cosAngDist_] = max(abs(cosAngDist_  ));
                        
        for i=1:database.size   
            % Get muscular activity in these positions (for all N entries)            
            musculardata(1,i) = database.muscInfoManip( cosAngDist_ ,i);                            
            penaltdata(1,i)   = database.penalty_selfCol( cosAngDist_ ,i);    
        end 
    end
    
end



%% Getting closest wrenches to the provided task-force (considering only 1 force : FLAG_wristFrame=false) 
if ~FLAG_wristFrame  
    
    if ~FLAG_NO_CONFIG     
        % 3 x N  dot product from forcevector (6xN) vector for each F force configuration
        % Results in a Ft x 1    and    Ff x 1   
        cosAngDist_torques = (database.configDataset.forces(1:3,database.configDataset.indTorques)'*force(1:3,1) );    
        cosAngDist_forces  = (database.configDataset.forces(4:6,database.configDataset.indForces)'*force(4:6,1) );    
        % Obatin the configuration closest to cos(0)=1 where 0 would be the exactly same force.  
        % Results indexes (1,1) for forces closest to prescribed force          
        [~, cosAngIndex_torques] = max(abs(cosAngDist_torques  ));
        [~, cosAngIndex_forces]  = max(abs(cosAngDist_forces   ));
        
         % Get muscular activity in these positions (for all N entries)
        musculardata_torques(1,:) = database.muscInfoManip(database.configDataset.indTorques(cosAngIndex_torques(i)), :);
        musculardata_forces(1,:)  = database.muscInfoManip(database.configDataset.indForces(cosAngIndex_forces(i)), :);
        penaltdata(1,:)  = database.penalty_selfCol( database.configDataset.indForces( cosAngIndex_forces(i) ) ,:); 
                        
    else
        % 3 x N  dot product from forcevector (6xN) vector for each F force configuration
        % Results in a F x N    
        cosAngDist_ = (database.configDataset.forces(:,:)'*force );    
        % Obatin the configuration closest to cos(0)=1 where 0 would be the exactly same force.  
        % Results indexes (1,N) for forces closest to prescribed force          
        [~, cosAngDist_] = max(abs(cosAngDist_  ));
        
        musculardata(1,:) = database.muscInfoManip( cosAngDist_ ,:);                            
        penaltdata(1,:)   = database.penalty_selfCol( cosAngDist_ ,:);                    
    end
    
end



if ~FLAG_NO_CONFIG   
    % Get maximum muscular trans rate
    max_MiTR = max( rmoutliers( musculardata_forces ) );
    musculardata_forces = min(musculardata_forces, max_MiTR);
    %musculardata_forces(1, musculardata_forces > max_MiTR) = max_MiTR;

    % Adjusting equal contributions for torque and force
    musculardata_torques = musculardata_torques*(max_MiTR /  max( rmoutliers( musculardata_torques ) ) );
    musculardata_torques = min(musculardata_torques, max_MiTR);

    musculardata = forceMag_force*musculardata_forces + forceMag_torque*musculardata_torques;    
           
else    
    
    % Get maximum muscular trans rate
    max_MiTR = max( rmoutliers( musculardata ) );
    musculardata = min(musculardata, max_MiTR);
    %musculardata(1, musculardata > max_MiTR) = max_MiTR;
end


% Getting muscular informed transmission rate data (normalized)
musculardata = musculardata/max(musculardata);        




%% Output Datastruct
tsDatabase.datasetSize = database.size;    
tsDatabase.joints      = database.joints;    
tsDatabase.pos         = database.pos;    
tsDatabase.rot         = database.rot;    
tsDatabase.ergoManip   = database.ergoManip;    
tsDatabase.muscInfoManip   = musculardata;
tsDatabase.penalty_selfCol = penaltdata;

tsDatabase.configDataset      = database.configDataset;
tsDatabase.rhumanModel        = obj.rhuman;
    
    
    
    
  

end
