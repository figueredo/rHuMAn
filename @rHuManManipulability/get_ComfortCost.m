%% Retrieves comfortCost (and:  [comfortCost, MuscleTransRate, RULA, penalties])
% 
% %--------------------------------------------------------------------------
%  This function returns the combined ergonomics (RULA) and muscular assessment 
%     * Muscular assessment from the kinematics & biomechanics according to 
%       Saul et al., "Benchmarking of dynamic simulation predictions in two 
%       software platforms using an upper limb musculoskeletal model Benchmarkin…", CMBBE, (2015). 
%        
%     * RULA/REBA points are computed according to 
%       McAtamney, L., & Hignett, S. (2004). Rapid Entire Body Assessment. Handbook 
%       of Human Factors and Ergonomics Methods, 31, 8-1-8–11. https://doi.org/10.1201/9780203489925.ch8 
% 
%  -------------------------------------------------------------
%  *** Note that z:height, x:sideways (right shoulder out), y:face-front 
%  ***           for the hand:  z:out of fingers, x:palm down, y:thumbs up
% 
% 
%#########################################################################
% 
%------------------------[ Outputs ]
%  * [comfortCost,  muscular-transmission-rate,  RULA_value]
% 
%       comfortCost : 
%               ranges from [0 -> 1] (from uncomfortable => max. comfortable) 
%               Values will vary according to gain kappa (input)
% 
%       muscular-transmission-rate :  1/max(alpha) where alpha = muscle-activity vector
% 
%       RULA: RULA/REBA points. Returns error when 0. 
% 
% 
%------------------------[ INPUT ]
%  * joints (joints): A 7x1 double with joint values in rad (according to Saul's model)  
%                    Default joints limits (can be changed): 
%                    Joint limits (lower):   [-90  0    -90   0        -90   -15  -75]'*(pi/180)
%                    Joint limits (upper):   [130  180   +45  145      +90   +25  +75]'*(pi/180)
% 
%  * forcevector (double 6,N) with N >= 1
%                    Defines the unique force or a set of forces to be analyzed.
%                    If N>1 (multiple-forces), then the output is also a [vector, vector, matrix]
%                 (double 3,N) : similar but with wrench defined by: [0;forcevector]                
% 
%  * kMuscle (double) : Gain between [0,1] to weight muscular (1) and ergonomics (0)
%                       Example 0.5 (equal weights), while 1 implies only muscular-assessment 
% 
% 
%------------------------[ Optional: INPUTs ]
% 
%  * maxTransRate (double(N): Maximum Transmission Rate per force analyzed
%                    (if N=1, the same value will be applied to all forces
%                    IF NOT DEFINED: Algorithm will search for an approximate value (increasing time) 
% 
% 
%------------------------[ Optional: INPUTs ] : [Format: String followed by values ]
% 
%  * 'wrist',logical      :  If forces are defined in the end-effector (not in DEFAULT: task-space). Default=false.  
%  * 'wristframe',logical :  same as above. Default=false.
%  * 'end',logical :  same as above. Default=false.
%  * 'endeffector',logical :  same as above. Default=false.
% 
%  * 'selfCollision',logical : Default: True : Add selfCollision penalties to the final cost. 
% % 
%------------------------[ Example ]
%  [comfortCost, MuscleTransRate, RULA, penalties] = get_ComfortCost(joints, force, 1.0)
%  [comfortCost, MuscleTransRate, RULA, penalties] = get_ComfortCost(joints, force_vec, 0.5, 'wrist',true)
%  [comfortCost, MuscleTransRate, RULA, penalties] = get_ComfortCost(joints, force_vec, 0.5, 250)
%  [comfortCost, MuscleTransRate, RULA, penalties] = get_ComfortCost(joints, force_vec, 0.5, 'selfCollision',false )
% 
% 
%######################################################################### 


%% Function fct_get_muscularCost
function [comfortCost, MuscleTransRate, RULA, penalties] = get_ComfortCost(obj, joints, forcevector, kMuscle, maxTransRate,  options  ) 
    %==============[ Check number of extra inputs ]
    arguments
        % Fixed Argument
        obj
        joints (7,1) double;    % A 7x1 double with joint values in rad (according to Saul's model)      
        forcevector double;     % (double 6,N) or (double 3,N) => Defines the unique force or a set of forces to be analyzed. (force or wrench)           
        kMuscle     double;     % Gain between [0,1] to weight muscular (1) and ergonomics (0)
        maxTransRate double = -1;
        %
        % Extra arguments [ check frame of assessment ] 
        options.wrist       logical = false;     % If forces are defined in the end-effector (not in DEFAULT: task-space). Default=false.  
        options.wristframe  logical = false;     % If forces are defined in the end-effector (not in DEFAULT: task-space). Default=false.  
        options.end         logical = false;     % If forces are defined in the end-effector (not in DEFAULT: task-space). Default=false.  
        options.endeffector logical = false;     % If forces are defined in the end-effector (not in DEFAULT: task-space). Default=false.         
        % Self Collision Checking
        options.selfCollision logical = true;     % Default: True : Add selfCollision penalties to the final cost.         
    end
    if kMuscle < 0 || kMuscle >1
        disp([10,9,'*** WARNING: Using a gain kMuscle (value: ',num2str(kMuscle),' ) different than [0,1]']);
        disp([10,9,'***          For test, the gain will be set to kMuscle=0.5.',10]);
        kMuscle = 0.5;
    end
        
        
    
    %% Check number of forces and they will be defined in end-effector.    
    forcesize = size(forcevector,2);
    
    if length(maxTransRate) == 1
        maxTransRate = maxTransRate*ones(1,forcesize);
    elseif length(maxTransRate) < forcesize
        error('In function: fct_getComfortCost. Variable maxTransRate must be a double of size equal to 1 or equal to the number of columns in forcevector')
    end
    
    % If only in 3D (assume that is force and not torques).
    if size(forcevector,1)==3
        forcevector = [zeros(3,forcesize); forcevector];
    end
    
    
    % Field exists (variable was provided) 
    if options.wrist || options.wristframe || options.end || options.endeffector 
        % Transforming force data to wrist frame 
        forcevector_original = forcevector;
        clear forcevector        
        xm = obj.rhuman.getFKM.getFKM(joints);  
        for i=1:forcesize
            forcevector(:,i) = vec6( xm*vec6(forcevector_original(:,i))*xm' );
        end        
    end
       
        

    
    %% Getting individual muscular and ergonomic values
    
    if options.selfCollision
        if obj.rhuman.checkSelfCollision(joints)
            comfortCost =-1;
            MuscleTransRate =0;
            RULA=0;
            penalties = 0;
        end
    end
    
    % Get ergonomic values (RULA)    
    RULA        = obj.get_RULA( joints );
    RULA_value  = obj.handleRula2ErgoIndex(RULA);           
    
    % Get muscular activity values
    normValue = sum((forcevector.^ 2),1).^0.5;        
    [~,MuscleTransRate,~, penalties ] = obj.get_muscularCost( joints, forcevector./normValue, 'wrist', obj.FLAG_wristFrame )   ;
    
    
    if maxTransRate<0
        disp([10,10,9,'*** Warning:  Variable: maxTransRate was not defined for this force input.'])
        stringtext=strcat([10,9,9,'***  Hence, we are searching the space to find some values to compare with.',10,9,9, ...
                            '***  This lead to longer assessment time.',10,9,9,'***  To improve speed, define the maximum Transmission Rate expected for the force (or set of forces (as an array)).']);
        obj.printv(obj,stringtext)
        it = 1;
        while it<100
            qj = obj.rhuman.getRandJoints();
            if obj.rhuman.checkSelfCollision(qj)
                continue;
            end                    
            [~,tmpmitr(it,:),~, ~ ] = obj.get_muscularCost( qj, forcevector./normValue, 'wrist', obj.FLAG_wristFrame );  
            it = it+1;
        end          
        for it=1:forcesize
            maxTransRate(it) = max(rmoutliers(tmpmitr(:,it),'ThresholdFactor',2.5));                
            disp(strcat([9,9,'*** Max Informed Transmission Rate was computed as: ',num2str(maxTransRate(it)),10,' ',10]))
        end

    end


    %% Getting the output comfort cost
    
    comfortCost = zeros(1,forcesize);    
    for i=1:forcesize
        comfortCost(i) = kMuscle*MuscleTransRate(i)/maxTransRate(i) + (1-kMuscle)*RULA_value;
        if options.selfCollision
            if penalties(i)==0
                comfortCost(i)=-1;
            else
                comfortCost(i) = penalties(i)*comfortCost(i); 
            end            
        end
    end    
    
end




