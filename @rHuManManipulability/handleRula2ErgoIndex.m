%% return Ergonomic cost from RULA Score
% 
% 
%--------------------------------------------------------------------------
%  This method is part of the class rHuManManipulability
%  -
% Compute Ergonomic Index from RULA SCORE
% 
%-------------------------------------------------------------------------- 
% Function inputs:
%       RULA SCORE   -  double
% 
% Function outputs:
%       ergoIndex    -  double with normalized Ergonomic Index [0,1]
%-------------------------------------------------------------------------- 
% 
%  ergoIndex = handleRula2ErgoIndex(RULA)
% 
%-------------------------------------------------------------------------- 



%% Get Ergonomic Index from RULA values
% 
function ergoIndex = handleRula2ErgoIndex(obj, RULA)
    nRula = ( (RULA-1)/obj.ergoMaxScore );
    ergoIndex = (1 + nRula*exp(nRula))^-2;        
end

