%% Build a Task-Specific Comfortability Dataset with Muscular-Informed Manip and Ergonomics  
% 
%--------------------------------------------------------------------------
%  This method is part of the class rHuManManipulability
%  -
%  It builds the task-specific comfortability manipulability distribution. 
%  See referenced paper for further details
%-------------------------------------------------------------------------- 
% 
%######################################################################### 
%------------------------[ Outputs ]
%  * datastruct:  returns the a datastruct with joints, pos, rot, ergonomics, muscular-informed manip, and penalties for human workspace. 
%                 Method also updates variables within the object (thus, no need to safe output if it is not going to be used)  
%
% 
%------------------------[ INPUTs: ]  
%  * datasize (double) : defines size of the datastruct (minimum is 100)  
% 
%  * force    (double(6,1) OR double(3,1)) : Defines the task-specific force for analysis. 
%                                            It accepts a Double(6,1) - for a wrench [torque;force] 
%                                            OR Double(3,1) - for a wrench [0;force] with only a force.
% 
%  * kMuscle (double) : Gain between [0,1] to weight muscular (1) and ergonomics (0)
%                       Example 0.5 (equal weights), while 1 implies only
%                       muscular-assessment [Default = 0.5 ]
% 
% 
% 
%------------------------[ Optional: INPUTs ]:[Format: String followed by values ]
% 
%  * 'wrist',logical            : If forces/torques are defined in the end-effector (not in DEFAULT: task-space). Default=false.  
%  * 'wristframe',logical       : same as above. Default=false.
%  * 'end',logical              : same as above. Default=false.
%  * 'endeffector',logical      : same as above. Default=false.
% 
% 
%------------------------[ EXAMPLES ]
% build_TSComfortability(300000,randn(6,1));
% build_TSComfortability(600000,randn(3,1));
% build_TSComfortability(600000,randn(3,1),'wrist');
% 
%######################################################################### 


%% Function fct_get_muscularCost
function tsComfDist = build_TSComfortability(obj, datasize, force, kMuscle, options)
    %==============[ Check number of extra inputs ]
    arguments
        obj
        % Extra arguments [ check frame of assessment ] 
        datasize            double;        % Defines size of the datastruct (minimum is 100)  
        force               double;        % Defines the task-specific force for analysis. It accepts a Double(6,1) - for a wrench [torque;force] OR Double(3,1) - for a wrench [0;force] with only a force.
        kMuscle             double=0.5;    % Gain between [0,1] to weight muscular (1) and ergonomics (0) 
        %
        options.wrist       logical = false;  % [Optional] If forces are defined in the end-effector (not in DEFAULT: task-space). Default=false.  
        options.wristframe  logical = false;  % [Optional] If forces are defined in the end-effector (not in DEFAULT: task-space). Default=false.  
        options.end         logical = false;  % [Optional] If forces are defined in the end-effector (not in DEFAULT: task-space). Default=false.  
        options.endeffector logical = false;  % [Optional] If forces are defined in the end-effector (not in DEFAULT: task-space). Default=false.         
    end
    
%% Check number minimum of entries: 10000
    if datasize <100
        error([10,9,'*** Number minimum for dataset lenght is 100. For values below use function getComfortValue'])
    end                

%% Check if force is defined over wrist or task-space(default)    
    if options.wrist || options.wristframe || options.end || options.endeffector 
        obj.FLAG_wristFrame = true;
    else
        obj.FLAG_wristFrame = false;
    end        
    
%% Set task-specific forces 

    % Transpose if column form
    if size(force,2)~=1,  force = force';    end
    
    % Building wrench 
    if size(force,1)==3,  force = [zeros(3,1); force]; end
    
    % Argument validator for wrench
    if length(force)~=6  
        error(['*** build_TSComfortability() function takes (datasize, force, [optional:''wrist'']) as input. Force is a wrench and must be defined as a double(6,1) or double(3,1).']);
    end
    
    if abs(1-norm(force)) > 10^-6
        obj.printv(obj,['*** Normalizing input force.'])
        force = force/norm(force);
    end
               
    
%%  Building data-struct

% Temporary Struct (not to lose the previous one in case of failure)
tempObj.datasetSize = datasize;
tempObj.joints      = zeros(7,datasize);
tempObj.pos         = zeros(3,datasize);
tempObj.rot         = zeros(4,datasize);
tempObj.ergoManip   = zeros(1,datasize);
tempObj.muscInfoManip   = zeros(1,datasize);
tempObj.penalty_selfCol = zeros(1,datasize);
   

% Configuration values
it = 1;
joints = obj.rhuman.getRandJoints('length',datasize);
FLAG_INVALID_JOINT = false;


ticstart = tic;
verboseVar = obj.verbose;
obj.verbose = false;
while(it <= datasize)    
    

    % Get random configuration
    %---------------------------------------   
    if FLAG_INVALID_JOINT
        joints(:,it) = obj.rhuman.getRandJoints();
    end
    FLAG_INVALID_JOINT = true;
    
    %---------------------------------------
        
    % Get muscular activity values   
    [~,MuscleTransRate_vec,~, penalties_vec ] = obj.get_muscularCost( joints(:,it), force, 'wrist', obj.FLAG_wristFrame );

    % Collision check
    if min(penalties_vec)==0
        continue;
    end
       
    xm  = obj.rhuman.getFKM(joints(:,it));
    pos = xm.translation.q(2:4);
    rot = xm.P.q(1:4);
    % Get ergonomic values (RULA)    
    RULA        = obj.get_RULA( joints(:,it) );
    RULA_value  = obj.handleRula2ErgoIndex(RULA);

                
    % Store data and go to next! 
    %---------------------------------------   
    FLAG_INVALID_JOINT = false;    
    tempObj.pos(:,it)  = pos;
    tempObj.rot(:,it)  = rot;
%     %----------
    tempObj.ergoManip(it)         = RULA_value;
    tempObj.muscInfoManip(:,it)   = MuscleTransRate_vec;
    tempObj.penalty_selfCol(:,it) = penalties_vec;
    % Update index
    it = it + 1;       
    
    percentageCompleted = it/datasize;
    if mod(percentageCompleted*100,5)==0
        disp(['*** Percentage completed: ',num2str(percentageCompleted*100),'% at ',num2str(toc(ticstart)),' sec'])
    end

end
obj.verbose = verboseVar;



%% Building comfortability distribution
 
%  Remove outliers from extreme forces
obj.voxConfig.maxMiTR = max( rmoutliers( tempObj.muscInfoManip,'percentiles',[5,95] ) );
tempObj.muscInfoManip( tempObj.muscInfoManip > obj.voxConfig.maxMiTR ) = obj.voxConfig.maxMiTR;
tmpMIn = min( rmoutliers( tempObj.muscInfoManip,'percentiles',[5,95] ) );
tempObj.muscInfoManip( tempObj.muscInfoManip < tmpMIn ) = tmpMIn;

% NOrmalize muscular informed transmission rate
tempObj.muscInfoManip = tempObj.muscInfoManip./obj.voxConfig.maxMiTR;

% Distance in voxalized data
diststep = obj.voxConfig.distanceCart;
diststep = sqrt(1*diststep^2);

% Get workspace
tmpxyz=[tempObj.pos(1,:)];  rangeXYZ{1} = min(tmpxyz) :diststep: max(tmpxyz);  % Range for X
tmpxyz=[tempObj.pos(2,:)];  rangeXYZ{2} = min(tmpxyz) :diststep: max(tmpxyz);  % Range for Y
tmpxyz=[tempObj.pos(3,:)];  rangeXYZ{3} = min(tmpxyz) :diststep: max(tmpxyz);  % Range for Z
IAll = tempObj.pos;


% Removing previous manipulability data
obj.datasetSize = 0;
obj.joints      = [];
obj.pos         = [];
obj.rot         = [];
obj.ergoManip   = [];
obj.muscInfoManip   = [];
obj.penalty_selfCol = [];



% Going over the workspace
databaseindex = 1:1:datasize;    
it = 0; 
for ix = rangeXYZ{1}
    for iy = rangeXYZ{2}
        for iz = rangeXYZ{3}
            
            % Check every inch!
            D = [ix; iy; iz];  
            % Distances to this vox
            distances = pdist2(D', IAll');            
            % Indexes of distances below threshold
            vectorValidDist = distances < diststep;             
            indexes = databaseindex(vectorValidDist);
            
            if ~isempty(indexes)
                it = it+1;
                              
                % Getting the best configuration among the ones that satisfies the voxalized constraint 
                [obj.comfortIndex(1,it), indexVox] = ...
                      max( tempObj.penalty_selfCol(vectorValidDist).* ...
                          ( kMuscle*tempObj.muscInfoManip(vectorValidDist) ...
                          + (1-kMuscle)*tempObj.ergoManip(vectorValidDist)  )  );
                                     
                obj.joints(:,it)      = joints(:,indexVox);
                obj.pos(:,it)         = D;
                obj.rot(:,it)         = tempObj.rot(:,indexVox);
                obj.ergoManip(:,it)       = tempObj.ergoManip(indexVox);
                obj.muscInfoManip(:,it)   = tempObj.muscInfoManip(:,indexVox);
                obj.penalty_selfCol(:,it) = tempObj.penalty_selfCol(:,indexVox);                       
            end
            
        end
    end
end

obj.datasetSize = it;
clear tempObj




%% Storing configuration 
obj.configDataset.forces     = force;
obj.configDataset.indForces  = [];
obj.configDataset.indTorques = [];
obj.configDataset.indJacobSVDvec = [];
obj.configDataset.torqueNormalization = [];
obj.configDataset.FLAG_wristFrame     = obj.FLAG_wristFrame;  



%% Data output

tsComfDist.size   = obj.datasetSize;
tsComfDist.joints = obj.joints;
tsComfDist.pos    = obj.pos;
tsComfDist.rot    = obj.rot;
tsComfDist.comfortIndex       = obj.comfortIndex;
tsComfDist.ergoManip          = obj.ergoManip;
tsComfDist.muscInfoManip      = obj.muscInfoManip;
tsComfDist.penalty_selfCol    = obj.penalty_selfCol;
tsComfDist.configDataset      = obj.configDataset;
tsComfDist.voxConfig          = obj.voxConfig;
tsComfDist.rhumanModel        = obj.rhuman;




end
