%% return RULA Score from Ergonomic Index
% 
% 
%--------------------------------------------------------------------------
%  This method is part of the class rHuManManipulability
%  -
% Compute RULA SCORE from Ergonomic Index 
% 
%-------------------------------------------------------------------------- 
% Function inputs:
%       ergoIndex    -  double with normalized Ergonomic Index [0,1]
% 
% Function outputs:
%       RULA SCORE   -  double
%-------------------------------------------------------------------------- 
% 
%  RULA = handleErgoIndex2RULA(ergoIndex)
% 
%-------------------------------------------------------------------------- 



%% Get Ergonomic Index from RULA values
% 
function RULA = handleErgoIndex2RULA(obj, ergoIndex)
    eps = 10^-5;
    for i = 1:14
        if abs(ergoIndex -obj.handleRula2ErgoIndex(i)) < eps
            RULA = i;
            break        
        end
    end      
end

