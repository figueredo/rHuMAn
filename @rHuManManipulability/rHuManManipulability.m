%% RapidHuman-Manipulability Assessment - ClassDef rHuManManipulability.m
%
% CLASS Definition for rHuManManipulability
%==========================================================================
% VERSION: VS. 0.3.0  =>  2020-06
% AUTHORS:  Luis F.C. Figueredo, Mehmet Dogar, Anthony Cohn 
%           University of Leeds
% Contact information: figueredo@ieee.org
%==========================================================================% 
% 
%  RETURNS OBJECT RapidHuman-Manipulability Assessment (rHuManManipulability)
% 
%  -------------------------------------------------------------
%  Human Manipulability and Comfortability defined accordingly to 
%  paper (arxiv version)
%  - 
% 
%% ====( Output and Inputs )=============================================================
% 
%------------------------[ OUTPUT ]
%  * rHuManManipulability object with methods for human manipulability
% 
%------------------------[ INPUT ]
%  * humanModel from the class rHuManModel 
% 
%                 
%------------------------[ INPUTs (OPTIONAL!) ]: [Format: String followed by values ]
% 
%------------[ Muscular-Assessment Method ]                
%   'opensim',logical :    [NOT-AVAILABLE IN THIS VERSION] Assessment using opensim connection directly from matlab (not implemented yet).
%   'localdata',char  :    DIR (absolute or relative) with relative or absolute path for dataset containing muscleMaxForce.mat and fct_getmuscleActivation.m functions 
%                 
%
% 
%% ====( Variables )=============================================================
%  rhuman - humanModel from the class rHuManModel 
%  datasetSize   - Number (N) of inputs in the dataset 
%  joints        - double(7,N) of N joint positions. For the human model see classdef rHuManModel       
%  pos           - double(3,N) of N task-space positions for the user hand
%  rot           - double(4,N) of N task-space orientation (quaternion) for the user hand
%  ergoManip     - double(1,N) of ergonomics manipulability (based on RULA or REBA scores)     
%  muscInfoManip - double(F,N) of N muscular-informed manipulability (MiM)  where)
%                  F=1 for general MiM and F = number of wrenches(force/torques)
%                   /accelerations for the augmented MiM (default: 58) 
%  penalty_selfCol   double (F,N) of self-collision penalty values for each N entry and 
%                    F force/acceleration directions. Penalties defined at  classdef rHuManModel    
% 
%  comfortIndex  - Resulting comfort index (only available when calling reshape functions)  
% 
%  Both muscInfoManip and penalty_selfCol size will be updated according     
%  to calls to buildComfortability or buildAugmentedComfortability functions
%  among other functions 
% 
%  voxConfig     - Voxalized data configuration (Cartesian distance (5cm default], rotationDivisions [10 default], maxMuscle-Transm Rate and MaxMuscle-Inform-Manip. to be defined)   
% 
%  configDataset - Configuration for current dataset (including forces and if they are defined in wrist frame or task-space) 
% 
%  verbose = false;
% 
% 
% 
%% ====( Methods )=============================================================
% 
%     build_AugmentedComfortDataset 
%              [dataset] = build_AugmentedComfortDataset(N, [options]);  
%                               => Builds the comfortability data-struct with N entries. N>100. Stores data-struct in object-class and returns data-struct (optionally)
%                               => Additionally: accepts the same inputs from (setForces4Manipulability) and 'default' to use the set of default forces (58)                     
% 
%     build_TSComfortability
%              [dataset] = build_TSComfortability(N, force, kMuscle, [options])
%                               => Builds the task-specific comfortability manipulability distribution with N entries. N>100. Stores data-struct in object-class and returns data-struct (optionally)
%                                  'force' is the task-specific force (or acceleration) direction. 
%                                  'kMuscle' is the gain between [0,1] to weight muscular (1) and ergonomics (0)
%                               => Optional: 'wrist',logical    : If forces/torques are defined in the end-effector (not in DEFAULT: task-space). Default=false.  
% 
%     compute_gainMuscle
%              [gain_Muscle] = compute_gainMuscle(Speed,Intensity,Repetitive)            
%                               => Compute muscular-gain for comfortability.  Ranges from [1]: only muscle to [0] only ergonomics
%                               => Inputs:  Speed      -   Ranges between [0,1] (for very slow to very fast task executions)
%                                           Intensity  -   Intensity ranging between expected minimum and maximum task-space forces [0,1]
%                                           Repetitive -   Repetitive has 3 modes: 0 (unique task, 0.5: repetes from time to time, 1 very repetitive (more than 4x per minute).   
% 
%     get_ComfortCost
%              [comfortCost, MuscleTransRate, RULA, penalties] = get_ComfortCost(joints, forcevector, kMuscle, maxTransRate, options)
%                               => Function compute comfort cost from both ergonomics and muscular (according to KMuscle). Ranges from [0 -> 1] (from uncomfortable => max. comfortable) 
%                                  MuscleTransRate (normalized). RULA score (least the better). 
%                                  Returns -1 if self-collision is detected (and if self-collision detected is enabled [default]) 
%                                  Input: Joints (double7,1); forcevector (either double(6,N) ou double(3,N);  
%                                         kMuscle: Gain between [0,1] to weight muscular (1) and ergonomics (0)   
%                                         maxTransRate: Max Transmission Rate (vector) per force analyzed. IF NOT DEFINED: Algorithm will search for an approximate value (increasing time)             
%                                  [Optional:Inputs]:  'wrist',logical         : If forces/torques are defined in the end-effector (not in DEFAULT: task-space). Default=false. 
%                                                      'selfCollision',logical : Default: True : Add selfCollision penalties to the final cost. 
% 
%     get_muscularCost
%              [muscleActEffort, MuscleTransRate, musclesAct, penalties] = get_muscularCost(joints, forcevector,  options  ) 
%                               => Returns: muscular-activity effort    => denotes the sqrt( alpha^T*alpha ) where alpha = muscle-activity vector)
%                                           muscular-transmission-rate  => 1/max(alpha)  
%                                           muscle-act-vector           => alpha = double (50,1) with muscle-activity values
%                                           penalties                   => Self-collision penalties vector (for each force)
%                                  Input: Joints (double7,1); forcevector (either double(6,N) ou double(3,N);  
%                                  [Optional:Inputs]:  'wrist',logical         : If forces/torques are defined in the end-effector (not in DEFAULT: task-space). Default=false. 
% 
%     get_RULA 
%              RULA_SCORE = get_RULA(joints);  Returns RULA score computed according to McAtamney, L., & Hignett, S. (2004). Rapid Entire Body Assessment. Handbook of Human Factors and Ergonomics Methods, 31, 8-1-8–11. https://doi.org/10.1201/9780203489925.ch8 
%                         = fct_get_RULA(joints, 'shoulderRaised',true)       => [optional: Extra pontuation for RULA (if shoulder is raised), see referenced paper for further details]            %                         
%                         = fct_get_RULA(joints, 'shoulderPos',[0.10 0 1.3])  => [optional: Extra pontuation for RULA (pos of shoulder to check if it is raised)]
%                         = fct_get_RULA(joints, 'armSupported', true)        => [optional: Extra pontuation for RULA (if arm is supported), see referenced paper for further details]     
%                         = fct_get_RULA(joints, 'repetitive', true)          => [optional: Extra pontuation for RULA (if task is repetitive), see referenced paper for further details]     
%                         = fct_get_RULA(joints, 'heldstatic', true)          => [optional: Extra pontuation for RULA (if task is held for long), see referenced paper for further details]     
%                         = fct_get_RULA(joints, 'abrupt', true)              => [optional: Extra pontuation for RULA (if task is abrupt), see referenced paper for further details]     
%                         = fct_get_RULA(joints, 'heavyload', true)           => [optional: Extra pontuation for RULA (if task involves heavy load (e.g., tool is heavy)), see referenced paper for further details]     
% 
%     get_TSComfortDataset
%              database = get_TSComfortDataset(force, options)       
%                               => Function outputs a task-specific comfortability dataset from augmented data-struct with muscular and ergonomics data 
%                               => 'force' is the task-specific force (or acceleration) direction.  
%                               => [Optional:Input]:  'wrist',logical         : If force/torque is defined in the end-effector (not in DEFAULT: task-space). Default=false. 
%                               => [Optional:Input]:  'external',augmentedDataStruct  : Explores an external datastruct instead of the one in the object itself [default].
%                                                       External datastruct must contain fields: {size,joints,pos,rot,ergoManip,muscInfoManip,penalty_selfCol,configDataset}   
% 
%     handleRula2ErgoIndex
%               ergoIndex = handleRula2ErgoIndex(RULA)  => Transform RULA to Ergonomic Index (normalized values)  
%     handleErgoIndex2RULA
%               RULA = handleErgoIndex2RULA(ergoIndex)  => Transform Ergonomic Index to RULA
% 
%     reshape_Comfortability
%              comfDist = reshape_Comfortability(kMuscle, options)       
%                               => Function reshapes (and outputs) a general comfortability distribution from exsiting augmented data-struct with muscular and ergonomics data 
%                               => KMuscle: Gain between [0,1] to weight muscular (1) and ergonomics (0) ==>  comfortability integrates muscular data and Ergonomics according KMuscle.
%                               => [Optional:Input]: 'external',augmentedDataStruct  : Explores an external datastruct instead of the one in the object itself [default].
%                                                     External datastruct must contain fields: {size,joints,pos,rot,ergoManip,muscInfoManip,penalty_selfCol}   
% 
%     reshape_TSComfortability
%              comfDist = reshape_Comfortability(force, kMuscle, options)       
%                               => Function reshapes (and outputs) a task-specific comfortability distribution from exsiting augmented data-struct with muscular and ergonomics data 
%                               => 'force' is the task-specific force (or acceleration) direction.  
%                               => [Optional:Inputs]:  'wrist',logical         : If force/torque is defined in the end-effector (not in DEFAULT: task-space). Default=false. 
%                               => Other inputs follow the same from (reshape_Comfortability)
% 
%     setForces4Manipulability
%              setForces4Manipulability() => Updates configuration forces to be used on comfortability data-struct construction. (default 58 wrenches). 
%              setForces4Manipulability('length') => defines size of force-vector (if below 52, all wrenches above 6 will be make random). Min: 6 (if below, use specific 'forces','torques','wrenches' entries). 
%              setForces4Manipulability('forces',forcevec)    => [forcevec:double(3,N)]  -> defines set of N forces for augmented manipulability assessment (This makes entry 'lenght' void) 
%              setForces4Manipulability('torques',torquevec)  => [torquevec:double(3,N)] -> defines set of N torques for augmented manipulability assessment (This makes entry 'lenght' void) 
%              setForces4Manipulability('forces',forcevec, 'torques',torquevec) => Defines both force and torques for assessment.   
%              setForces4Manipulability('wrenches',wrenchvec) => [wrenchvec:double(6,N)] -> defines set of N wrenches=[torque;force] for augmented manipulability assessment (This makes previous entries void) 
%              setForces4Manipulability('wrist',true) => Defines reference frame of the forces to be in-hand (DEFAULT: false which implies forces defined in task-space)
%                                                        Alternative names: 'wristframe','end','endeffector'.   
%                                                        Can be combined with previous entries (e.g.,  setForces4Manipulability('endeffector',true,'forces',forcevec)
%
%
%% ====( TODO  )=============================================================
%      * 
%      * 
% 
%% ====( CHANGES LOG )=======================================================
%%%[ 2019-09-01 ]%%=> Functions for muscular manipulability assessment created and tested
%%%[ 2020-06-21 ]%%=> rHuManManipulability.m created over existing projects
%%%[ 2020-06-30 ]%%=> rHuManManipulability published




%% DEFINE CLASS BASICS:    rHuManManipulability()
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
classdef rHuManManipulability < handle

    
%% PROPERTIES    
    properties
        rhuman = [];
        datasetSize = 0;
        joints = [];
        pos = [];
        rot = [];
        ergoManip       = [];         
        %augmentedMiM  = [];
        muscInfoManip   = [];         
        penalty_selfCol = [];
        comfortIndex  = [];
        
        
        % Configuration for current dataset
        configDataset = struct('forces',[],'indForces',[],'indTorques',[],'torqueNormalization',[],'FLAG_wristFrame',[]);        
                
        % Voxalized data configuration (Cartesian distance (5cm default], rotationDivisions [10 default], maxMuscle-Transm Rate and MaxMuscle-Inform-Manip. to be defined)   
        voxConfig = struct('distanceCart',0.05,'rotDiv',10,'maxMiTR',0,'maxMiM',0);        
        
        % Verbose
        verbose = false;

    end           
    
    properties (Access = private)         
        printbar = '*************************************************';
        print3   = '***';
        % CONSTANT MATRICES
        C8  = diag([1 -ones(1,3) 1 -ones(1,3)]');
        C4m = [];
        
        % Address to muscular assessment functions and database
        address2opensimdata = [];
        
        % Vector of forces
        configForces = [];
        % Index of force and torque vectors (division from force to torque): 
        % only for augmented manipulability
        indForces  = [];        
        indTorques = [];        
        indJacobSVDvec =[];
        % Torque normalization (only for augmented manipulability)
        torqueNormalization = [];        
        
        % Defines if assessment should be made with wrist-frame forces or world-frame (default) 
        FLAG_wristFrame = 0; 
              
        % Maximum value for RULA-REBA SCORE (for normalization
        ergoMaxScore = [];

    end

    
    
    
    
%% METHODS    
    methods 
        %*************************=========================================
        %************************* MAIN CLASS
        %*************************=========================================               
        function obj = rHuManManipulability( humanModel, options  )
            
            % Check number of extra inputs
            %===============================
            arguments
                humanModel
                %------------[ Muscular-Assessment Method ]                
                options.opensim     logical = false;  % Assessment using opensim connection directly from matlab (not implemented yet).
                options.localdata   char    = '';   % Assessment using localdatabase      
     
                % Extra arguments [ verbose ]                                                                    
                options.verbose  logical  = false;     % Verbose [bool]: print outputs (particularly extras) 
            end                 
            %=================%=================%
            
            %===============[ Argument Validator for class rHuManModel ]
            if ~isa(humanModel,'rHuManModel')
                error([10, '*** Input must be of class: rHuManModel'])
            end
            obj.rhuman = humanModel;
            
            %===============[ VERBOSE ]
            obj.C4m = obj.C8(1:4,1:4);  
            obj.verbose = options.verbose;   
            printstring = strcat(10,obj.print3,' ',obj.print3,' Human Manipulability Loaded.');            
            obj.printv( obj,printstring );
            %=================%=================%            
            
            
            %========== [ Argument Validator for database for muscular analysis]
            if options.opensim
                error([10, '*** Connection directly to opensim not implemented in this version. Please use the local-dataset provided.', ...
                       10,9,'Command ''newrhuManip = rHuManManipulability(humanModel, ''localdata'',''./OpenSimData'')'' ', ...
                       10,9,'Replace url with relative or absolute path for dataset containing muscleMaxForce.mat and fct_getmuscleActivation.m functions',10])
            end
            %===========
            if strcmp(options.localdata,'')
                error([10, '*** Please provide the local-dataset: ', ...
                       10,9,'Command ''newrhuManip = rHuManManipulability(humanModel, ''localdata'',''./OpenSimData'')'' ', ...
                       10,9,'Replace dir with relative or absolute path for dataset containing muscleMaxForce.mat and fct_getmuscleActivation.m functions',10])
            end
            %===========
            obj.address2opensimdata = options.localdata; 
            if exist(obj.address2opensimdata,'dir')==7
                obj.printv(obj,'*** Copying data to workspace and enabling functions for assessment.')
            else
                error([10, '*** DIR: ',obj.address2opensimdata,' not identified as a valid folder.',10])
            end
            if ~exist('fct_getmuscleActivation','file')==2
                error([10, '*** DIR : ',obj.address2opensimdata,' does not contains function fct_getmuscleActivation.',10])
            end
            %===========[ adding path ]
            addpath(obj.address2opensimdata)             
            %===========[ Loading muscleMaxForce ]
            if evalin('base','exist(''muscleMaxForce'',''var'')')==1
                obj.printv(obj,'*** muscleMaxForce already loaded in workspace with muscular-force database from opensim.')
            else          
                muscledatastring = strcat(obj.address2opensimdata,'/muscleMaxForce.mat');
                if exist(muscledatastring)
                    obj.printv(obj,'*** loading muscleMaxForce to  workspace with muscular-force database from opensim.....')
                    evalin('base',strcat('load(''',muscledatastring,''')'));                       
                else
                    error([10, '*** DIR : ',muscledatastring,' does not contains muscleMaxForce database.',10])
                end
            end
            
            
            
            %========== [ Building default set of forces for manipulability analysis]            
            obj.setForces4Manipulability( );
            obj.ergoMaxScore = 12;
            obj.FLAG_wristFrame = false;
            obj.printv(obj,'*** System configured to be used.')            
            disp(' ')
            
            

                        
        end

        
        
        
        
    %*************************=========================================
    %************************* PUBLIC METHODS    
    %*************************=========================================            
        % Set ForceVector for Muscular Comfortability Analysis
        setForces4Manipulability(obj, options)
        
        % Build Augmented comfortability dataset 
        database = build_AugmentedComfortDataset(obj, size, options) % 

        % Build Task-Specific Comfortability distribution
        database = build_TSComfortability(obj, datasize, force, kMuscle, options)        

        % Retrives a Comfortability distribution dataset 
        % Requires existing data-struct (but one can also be passed as an input)
        comfDist = reshape_Comfortability(obj, kMuscle, options)         
        
        % Retrives a Task-Specific Comfortability distribution
        tscomfDist = reshape_TSComfortability(obj, force, kMuscle, options)  
        
        % Gets a Task-Specific Comfortability database from an Augmented comfortability dataset 
        database = get_TSComfortDataset(obj, force, options)          

        % Function get RULA        
        RULA = get_RULA(obj, joints,  options ) 
        ergoIndex = handleRula2ErgoIndex(obj, RULA)
        RULA = handleErgoIndex2RULA(obj, RULA)
        % UPDATE FUNCTION:  updateRULA(actions)

        % Function get Muscular-Informed Metrics        
        [muscleActEffort, MuscleTransRate, musclesAct, penalties] = get_muscularCost(obj, joints, forcevector,  options  ) 

        % Function compute comfort cost from both ergonomics and muscular (according to KMuscle) 
        [comfortCost, MuscleTransRate, RULA, penalties] = get_ComfortCost(obj, joints, forcevector, kMuscle, maxTransRate, options)               


%===============[ TODO ] %===============   
        % Plot a Comfortability distribution
        plot_comfortability(obj, datastruct, options) 
%===============%===============%===============  

    end
    
    
    
    %*************************=========================================
    %************************* STATIC INTERN METHODS:
    %*************************=========================================             
    methods (Static)  
        % Method for computing gain kMuscle: Gain between [0,1] to weight muscular (1) and ergonomics (0)   
        [gain_Muscle] = compute_gainMuscle(Speed,Intensity,Repetitive)            
    end
       
    

    
    %*************************=========================================
    %************************* STATIC INTERN METHODS:
    %*************************=========================================             
    methods (Static, Access = protected)  
        % Custom validator functions
        mustBeA(input,className)        
        mustBeOfSize(input,rowSize)
        
        % Print data only if validation is on (verbose)
        printv(obj,string)

           
        
    end            
        

end

%%




