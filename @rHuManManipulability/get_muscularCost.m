%% Retrieves Muscular-Cost : [muscleActEffort, MuscleTransRate, musclesAct-vec, penalties]
% 
% %--------------------------------------------------------------------------
%  This method is part of the class rHuManManipulability
%  -
%  This function returns muscular assessment from the kinematics & biomechanics according to 
%  Saul et al., "Benchmarking of dynamic simulation predictions in two software platforms using an upper limb musculoskeletal model Benchmarkin…", CMBBE, (2015).   
%  -------------------------------------------------------------
%  *** Note that z:height, x:sideways (right shoulder out), y:face-front 
%  ***           for the hand:  z:out of fingers, x:palm down, y:thumbs up
% 
%--------------------------------------------------------------------------
% 
%#########################################################################
%------------------------[ Output ]
% 
%  muscular-activity effort     : ranges from [0 ->no effort to sqrt(ones(50,1)'*ones(50,1) )]              
%                                 denotes the sqrt( alpha^T*alpha ) where alpha = muscle-activity vector  
% 
%  muscular-transmission-rate   :  1/max(alpha)  
% 
%  muscle-act-vector            : double (50,1) with muscle-activity values
% 
%  penalties                    : self-collision penalties vector (for each force)
% 
% 
%------------------------[ INPUT ]
%  * joints (joints): A 7x1 double with joint values in rad (according to Saul's model)  
%                    Default joints limits (can be changed): 
%                    Joint limits (lower):   [-90  0    -90   0        -90   -15  -75]'*(pi/180)
%                    Joint limits (upper):   [130  180   +45  145      +90   +25  +75]'*(pi/180)
% 
%  * forcevector (double 6,N) with N >= 1
%                    Defines the unique force or a set of forces to be analyzed.
%                    If N>1 (multiple-forces), then the output is also a [vector, vector, matrix]
% 
%------------------------[ Optional: INPUTs ]
% [Format: String followed by values ]
% 
%  * 'wrist',logical      :  If forces are defined in the end-effector (not in DEFAULT: task-space). Default=false.  
%  * 'wristframe',logical :  same as above. Default=false.
%  * 'end',logical :  same as above. Default=false.
%  * 'endeffector',logical :  same as above. Default=false.
% 
%------------------------[ Example ]
%  [muscleActEffort, MuscleTransRate, musclesAct, penalties]  = get_muscularCost(joints, force)
%  [muscleActEffort, MuscleTransRate, musclesAct, penalties]  = get_muscularCost(joints, force_vec, 'wrist',true)
% 
%######################################################################### 


%% Function fct_get_muscularCost
function [muscleActEffort, MuscleTransRate, musclesAct, penalties] = get_muscularCost(obj, joints, forcevector,  options  ) 
    %==============[ Check number of extra inputs ]
    arguments
        obj
        % Fixed Argument
        joints (7,1) double;
%         rhumanmodel;
        forcevector double;        
        % Extra arguments [ check frame of assessment ] 
        options.wrist       logical = false;     % If forces are defined in the end-effector (not in DEFAULT: task-space). Default=false.  
        options.wristframe  logical = false;     % If forces are defined in the end-effector (not in DEFAULT: task-space). Default=false.  
        options.end         logical = false;     % If forces are defined in the end-effector (not in DEFAULT: task-space). Default=false.  
        options.endeffector logical = false;     % If forces are defined in the end-effector (not in DEFAULT: task-space). Default=false.         
    end
    
    %% Check number of forces and they will be defined in end-effector.    
    forcesize = size(forcevector,2);
    % If only in 3D (assume that is force and not torques).
    if size(forcevector,1)==3
        forcevector = [zeros(3,forcesize); forcevector];
    end

    % Field exists (variable was provided)    
    if options.wrist || options.wristframe || options.end || options.endeffector 
        % Transforming force data to wrist frame 
        forcevector_original = forcevector;
        clear forcevector        
        xm = obj.rhuman.getFKM(joints);  
        for i=1:forcesize
            forcevector(:,i) = vec6( xm*vec6(forcevector_original(:,i))*xm' );
        end        
    end

    
    %% Adding proper path for analysis
    %addpath('comfortMap_OpenSimData')        
    
    %% Getting torques
    %
    penalties = obj.rhuman.getSelfCollisionPenalties(joints, forcevector);
    geomJ = obj.rhuman.getJacobGeom(joints); 
    gettorques    = zeros(7,forcesize);
    for i=1:forcesize
        gettorques(:,i) = geomJ'*forcevector(:,i);                                   
    end         
    

    %% Compute muscular data

    muscleActEffort     = 50*ones(1,forcesize);
    MuscleTransRate     = zeros(1,forcesize);
    musclesAct          = zeros(50,forcesize);
    
    %getmaxTorqueCap = fct_getMaxTorqueCapability_perJoint( joints, muscleMaxForce);
    getmaxTorqueCap = fct_getMaxTorqueCapability_perJoint( joints, evalin('base', 'muscleMaxForce'));
    
    for i=1:forcesize
       [musclesAct(:,i), muscleActEffort(i)]    = fct_getmuscleActivation( gettorques(:,i) ,  getmaxTorqueCap );
       MuscleTransRate(i) = 1/max(musclesAct(:,i));                      
    end
end




