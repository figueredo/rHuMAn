%% Set ForceVector for Muscular Comfortability Analysis
% 
%--------------------------------------------------------------------------
%  This method is part of the class rHuManManipulability
%  -
%  It updates the set of forces used when building (augmented)
%  muscular-informed manipulability
%-------------------------------------------------------------------------- 
% 
%#########################################################################
%------------------------[ Outputs ]
%  * VOID:  Update list for forcevector - class variable 
% 
%------------------------[ Optional: INPUTs ]
% [Format: String followed by values ]
% 
%  * 'length',double        : defines size of forces for assessment (if below 52, all wrenches above 6 will be make random). Min: 6 (if below, use specific 'forces','torques','wrenches' entries). 
%  * 'forces',double(3,N)   : defines set of N forces for augmented manipulability assessment (This makes entry 'lenght' void)   
%  * 'torques',double(3,N)  : defines set of N torques for augmented manipulability assessment (This makes entry 'lenght' void)   
%  * 'wrenches',double(6,N) : defines set of N wrenches [torque;force] for augmented manipulability assessment (This makes entries 'lenght','forces','torques' void) 
%  * 'wrist',logical            : If forces/torques are defined in the end-effector (not in DEFAULT: task-space). Default=false.  
%  * 'wristframe',logical       : same as above. Default=false.
%  * 'end',logical              : same as above. Default=false.
%  * 'endeffector',logical      : same as above. Default=false.
% 
% 
%------------------------[ EXAMPLES ]
% setForces4Manipulability(); 
% setForces4Manipulability('length',30);
% setForces4Manipulability('forces',randn(3,30),'torques',randn(3,10));
% setForces4Manipulability('wrenches',randn(6,30));
% setForces4Manipulability('forces',randn(3,6),'wrist');
% 
%######################################################################### 


%% Function fct_get_muscularCost
function setForces4Manipulability( obj,  options  ) 
    %==============[ Check number of extra inputs ]
    arguments
        obj
        % Extra arguments [ check frame of assessment ] 
        options.length   double  = -1;        % Defines the number of forces to be analyzed when building manipulability (Note it has to more than 6). 
%         options.forces   options.forces {mustBeOfSize(options.forces,3)}     % Double(3,N) - Defines set of N forces for augmented manipulability assessment (This makes entry 'lenght' void)   
%         options.torques  options.torques {mustBeOfSize(options.torques,3)}   % Double(3,N) - Defines set of N torques for augmented manipulability assessment (This makes entry 'lenght' void)   
%         options.wrenches options.wrenches {mustBeOfSize(options.wrenches,6)} % Double(6,N) - Defines set of N wrenches [torque;force] for augmented manipulability assessment (This makes entries 'lenght','forces','torques' void)         
%         
        options.forces      double    % Double(3,N) - Defines set of N forces for augmented manipulability assessment (This makes entry 'lenght' void)   
        options.torques     double    % Double(3,N) - Defines set of N torques for augmented manipulability assessment (This makes entry 'lenght' void)   
        options.wrenches    double    % Double(6,N) - Defines set of N wrenches [torque;force] for augmented manipulability assessment (This makes entries 'lenght','forces','torques' void) 
        %
        options.wrist       logical = false;  % If forces are defined in the end-effector (not in DEFAULT: task-space). Default=false.  
        options.wristframe  logical = false;  % If forces are defined in the end-effector (not in DEFAULT: task-space). Default=false.  
        options.end         logical = false;  % If forces are defined in the end-effector (not in DEFAULT: task-space). Default=false.  
        options.endeffector logical = false;  % If forces are defined in the end-effector (not in DEFAULT: task-space). Default=false.         
    end

    
    
%% Check if force is defined over wrist or task-space(default)    
    if options.wrist || options.wristframe || options.end || options.endeffector 
        obj.FLAG_wristFrame = 'true';
    else
        obj.FLAG_wristFrame = 'false';
    end     

%% Defining forces from input    

    % Defining forces from user input wrenches
    if isfield(options,'wrenches')
        obj.mustBeOfSize(options.wrenches,6);
        obj.configForces = options.wrenches;
        obj.indForces  = [];        
        obj.indTorques = [];          
        options.length = -2;        
    else
        % Defining forces from user input forces
        if isfield(options,'forces')    
            obj.mustBeOfSize(options.forces,3);
            if options.length==-2    
                presize = size(obj.configForces,2);
                obj.configForces = [obj.configForces, [zeros(3,size(options.forces,2)); options.forces]];
                obj.indForces  = [presize+1:size(obj.configForces,2)];
            else
                obj.configForces = [zeros(3,size(options.forces,2)); options.forces];
                obj.indForces  = [1:size(options.forces,2)]; 
                obj.indTorques = [];
            end                  
            options.length = -2;
        end
        % Defining forces from user input torques
        if isfield(options,'torques')
            obj.mustBeOfSize(options.torques,3) ;           
            if options.length==-2
                presize = size(obj.configForces,2);
                obj.configForces = [obj.configForces, [options.torques; zeros(3,size(options.torques,2))]];
                obj.indTorques  = [presize+1:size(obj.configForces,2)];
            else
                obj.configForces = [options.torques; zeros(3,size(options.torques,2))];
                obj.indTorques = [1:size(options.torques,2)];    
                obj.indForces  = [];
            end            
            options.length = -2;
        end       
    end
    
    % Force already defined leave function 
    if options.length == -2
        obj.torqueNormalization = ones(1,size(obj.configForces,2));
        for i=1:size(obj.configForces,2)
           if norm(obj.configForces(:,i))~=1
               obj.configForces(:,i) = obj.configForces(:,i)/norm(obj.configForces(:,i));
               obj.printv(obj,'*** Warning: Input wrench/force/torque was not normalized. Database is build over normalized forces. Normalization has been performed internally.')
               if norm(obj.configForces(4:6,i))==0
                    obj.indTorques = [obj.indTorques i];
               elseif norm(obj.configForces(1:3,i))==0
                   obj.indForces = [obj.indForces i];
               end
           end
        end
        obj.indJacobSVDvec = [];
        return
    end
    
    
%% Defining forces from user-defined lenght
    if options.length > 0
        if options.length < 6
            disp([10,9,'*** Warning: Number of forces in variable ''length'' was below 6. Redifining it to 6.'])
            N = 6;
        else
            N = options.length;
        end
             

        if N>=52
            % Set the default forces (N=52)
            [obj.configForces,obj.indForces,obj.indTorques,obj.torqueNormalization]  = getCustomWrenches();
            obj.indJacobSVDvec = [];
            % Set the augmented default forces (N=58)
            if N>=58
                % Configure forces to be assessed from output vectors (svd)
                obj.configForces(:,53:55) = 50*[zeros(3,3); ones(3,3)];            
                obj.configForces(:,56:58) = 50*[ones(3,3); zeros(3,3)]; 
                obj.indForces  = [obj.indForces,  [53:55]];        
                obj.indTorques = [obj.indTorques, [56:58]];   
                obj.torqueNormalization = [obj.torqueNormalization, ones(1,6)];
                obj.indJacobSVDvec = [53:58];
            end

        elseif N<=6 
            % Configure forces to be assessed from output vectors (svd)
            obj.configForces(:,1:3) = 50*[zeros(3,3); ones(3,3)];            
            obj.configForces(:,4:6) = 50*[ones(3,3); zeros(3,3)]; 
            obj.indForces  = [1:3];        
            obj.indTorques = [4:6];         
            obj.torqueNormalization = [ones(1,6)];
            obj.indJacobSVDvec = [1:6];
        end
        
        % Getting Random forces (for input above already set wrenches)
        numWrenches   = size(obj.configForces,2);
        extraWrenches = N-numWrenches;
        % If there are extra wrenches to be set (random)
        if extraWrenches > 0
            extraForces  = ceil(extraWrenches/2);
            for i=(numWrenches+1):(numWrenches+extraForces)
                obj.configForces(:,i) = [zeros(3,1); randn(3,1)];  
                obj.configForces(:,i) = obj.configForces(:,i)/norm(obj.configForces(:,i));
                obj.indForces  = [obj.indForces, i];   
                obj.torqueNormalization = [obj.torqueNormalization 1];
            end            
            extraTorques = extraWrenches-extraForces;
            for i=(numWrenches+extraForces+1):(numWrenches+extraForces+extraTorques)
                obj.configForces(:,i) = [randn(3,1); zeros(3,1)];  
                obj.configForces(:,i) = 0.3*obj.configForces(:,i)/norm(obj.configForces(:,i));
                obj.indTorques  = [obj.indTorques, i];        
                obj.torqueNormalization = [obj.torqueNormalization 0.3];
            end
        end
             
        % Return as forces have been already defined from user-defined lenght
        return
    end    
           
    
%% Create Default vector forces (total 58)

% If code gets here is because there was no user input (default)
[obj.configForces,obj.indForces,obj.indTorques,obj.torqueNormalization]  = getCustomWrenches();
% Set the augmented default forces (N=58)

% Configure forces to be assessed from output vectors (svd)
obj.configForces(:,53:55) = 50*[zeros(3,3); ones(3,3)];            
obj.configForces(:,56:58) = 50*[ones(3,3); zeros(3,3)]; 
obj.indForces  = [obj.indForces,  [53:55]];        
obj.indTorques = [obj.indTorques, [56:58]];   
obj.torqueNormalization = [obj.torqueNormalization, ones(1,6)];
obj.indJacobSVDvec = [53:58];         
    
 
    
end



%% Get Custom set of forces
function [forcevector,indForces,indTorques,TORQUE_NORMALIZATION]  = getCustomWrenches()
    forcevector = zeros(6,52);
    TAUnormalization = 0.3;
    
    % Number of wrenches (it)
    it = 0;
    
    % Setting torque to zero, compute unit forces 
    tx = 0;     ty = 0;     tz = 0;    
    for fx = [-1, 0, 1]
        for fy = [-1, 0, 1]
            for fz = [-1, 0, 1]
                forcevec = [tx;ty;tz;  fx;fy;fz;  ];  
                if norm(forcevec)==0 
                    continue;
                end
                it = it +1;                          
                forcevector(1:3,it) = [tx;ty;tz]; 
                forcevector(4:6,it) = [fx;fy;fz]; 
                forcevector(:,it) = forcevector(:,it)/norm(forcevec);
            end
        end
    end
    indForces  = [1:it];
    indTorques = [it+1:size(forcevector,2)];
    
    % Setting forces to zero, compute unit torques
    fx=0;fy=0;fz=0; 
    for tx = [-1, 0,  1]
        for ty = [-1, 0,  1]
            for tz = [-1, 0,  1]
                forcevec = [tx;ty;tz;  fx;fy;fz;  ];  
                if norm(forcevec)==0 
                    continue;
                end
                it = it +1;                          
                forcevector(1:3,it) = [tx;ty;tz]; 
                forcevector(4:6,it) = [fx;fy;fz]; 
                forcevector(:,it) = TAUnormalization*forcevector(:,it)/norm(forcevec);

            end            
        end
    end   
    
    % output torque normalization
    TORQUE_NORMALIZATION = [ones(1,26), TAUnormalization*ones(1,26)];
    
end




