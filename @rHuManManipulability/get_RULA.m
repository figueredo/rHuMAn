%% Retrieves RULA / REBA Scores
% 
% %--------------------------------------------------------------------------
%  This method is part of the class rHuManManipulability
%  -
%  This function returns RULA (or REBA) assessment (points) taking the
%  kinematics from (fctLoadHuman_opensim_simplified) according to 
%  Saul et al., "Benchmarking of dynamic simulation predictions in two software platforms using an upper limb musculoskeletal model Benchmarkin…", CMBBE, (2015).   
%  - 
%  RULA points are computed according to 
%  McAtamney, L., & Hignett, S. (2004). Rapid Entire Body Assessment. Handbook of Human Factors and Ergonomics Methods, 31, 8-1-8–11. https://doi.org/10.1201/9780203489925.ch8 
% 
%  *** Note that z:height, x:sideways (right shoulder out), y:face-front 
%  ***           for the hand:  z:out of fingers, x:palm down, y:thumbs up
% 
%--------------------------------------------------------------------------
% 
%#########################################################################
%------------------------[ Output ]
%  * RULA: RULA points (Results between 1-13 ==>  Best=1, Worst=13). Returns error when 0. 
% 
%------------------------[ INPUT ]
%  * joints: A 7x1 double with joint values in rad (according to Saul's model)  
%                    Default:  (this can vary from classdef rHumanModel)
%                    Joint limits (lower):   [-90  0    -90   0        -90   -15  -75]'*(pi/180)
%                    Joint limits (upper):   [130  180   +45  145      +90   +25  +75]'*(pi/180)
% 
%------------------------[ Optional: INPUTs ]
% [Format: String followed by values ]
%  * 'shoulderRaised',boolean :  If Shoulder is raised 
%  * 'armSupported',boolean   :  If arm is supported 
%  * 'shoulderPos',vec3   :  Used to check if shoulder is raised (together with shoulderBase)
% 
%------------------------[Activities score ]
%  * 'repetitive',boolean   :  [Default: false] Extra pontuation for RULA (if task is repetitive), see referenced paper for further details  
%  * 'heldstatic',boolean   :  [Default: false] Extra pontuation for RULA (if task is held for long)
%  * 'abrupt',boolean       :  [Default: false] Extra pontuation for RULA (if task is abrupt)
%  * 'heavyload',boolean    :  [Default: false] Extra pontuation for RULA (if task involves heavy load (e.g., tool is heavy))
% 
% 
        %           [ deprecated ] * 'shoulderBase',vec3  :  Used to check if shoulder is raised 
        %           [ deprecated ] * 'handCrossLine',boolean   :  If working across midline of the body (chest)
        %           [ deprecated ] * 'handOutSideBody',boolean :  If arm out to side of body (check wrt to elbow) 
        %           [ deprecated ] * 'elbowPos',vec3 :  Used to check if wrist is crossing or outside body (together with wristPos and shoulderPos(OR)shoulderBase)
        %           [ deprecated ] * 'wristPos',vec3 :  Used to check if wrist is crossing or outside body (together with elbowPos and shoulderPos(OR)shoulderBase)
        %         
        %           [ deprecated ] * 'kine',DQ_kinematics :  Passing the DQ_Kinematics of the human upper limb allow the function to compute
        %                            (1) the shoulderBase; (2) elbowPos; and (3) wristPos which in turn allow the function to compute 
        %                            (a) if shoulderRaised (with shoulderPos); (b) if handCrossLine or handOutSideBody 
        %           [ deprecated ] * 'verbose',boolean   :  Print all points and steps
% 
% 
%------------------------[ Example ]
%  RULA_SCORE = fct_get_RULA(joints)
%  RULA_SCORE = fct_get_RULA(joints, 'shoulderRaised',true) 
%  RULA_SCORE = fct_get_RULA(joints, 'shoulderPos',[0.10 0 1.3], 'armSupported', true) 
% 
        %       [ deprecated ] * fct_get_RULA(joints, 'handOutSideBody',true, 'shoulderBase',[0 0 1.350],'shoulderPos',[0 0 1.375])  % which is true for shoulderRaised 
        %       [ deprecated ] * fct_get_RULA(joints, 'elbowPos',[0.15 0 0],'wristPos',[0.16 0 0])                        % which is true for handOutSideBody      
        %       [ deprecated ] * fct_get_RULA(joints, 'wristPos',[-0.30 0.10 1.3], 'shoulderPos',[-0.09 0 1.3])           % which is true for handCrossLine        
        %       [ deprecated ] * fct_get_RULA(joints, 'kine',humankine, 'shoulderPos',[0.10 0 1.3])     % Using the DQ_kinematics variable to compute  
%                                                                                                      % (1) the shoulderBase; (2) elbowPos; and (3) wristPos
% 
%#########################################################################


%% Function RULA
function RULA = get_RULA(obj, joints,  options ) 
    %% Check number of extra inputs
    arguments
        % Fixed Argument
        obj
        joints (7,1) double;
        
        % Extra arguments [ Shoulder ] 
        options.shoulderRaised  logical = false;     % shoulderRaised [bool]: +1 if Shoulder is raised 
        options.shoulderPos     double  = [0,0,0]';  % shoulderPos    [vec3]: used to check if shoulder is raised (together with shoulderBase)
        options.shoulderBase    double  = [0,0,0]';  % shoulderBase   [vec3]: used to check if shoulder is raised
        options.armSupported    logical = false;     % armSupported   [bool]: -1 if arm is supported 
        % Extra arguments [ Elbow ]  
        options.handCrossLine   logical = false;     % handCrossLine   [bool]: +1 if working across midline of the body (chest)
        options.handOutSideBody logical = false;     % handOutSideBody [bool]: +1 if arm out to side of body (check wrt to elbow) 
        options.elbowPos  double = [0,0,0]';         % elbowPos   [vec3]: used to check if wrist is crossing or outside body (together with wristPos and shoulderPos(OR)shoulderBase)
        options.wristPos  double = [0,0,0]';         % wristPos   [vec3]: used to check if wrist is crossing or outside body (together with elbowPos and shoulderPos(OR)shoulderBase)
        % Activities score 
        options.repetitive   logical = false;     % [Default: false] Extra pontuation for RULA (if task is repetitive), see referenced paper for further details  
        options.heldstatic   logical = false;     % [Default: false] Extra pontuation for RULA (if task is held for long)
        options.abrupt       logical = false;     % [Default: false] Extra pontuation for RULA (if task is abrupt)
        options.heavyload    logical = false;     % [Default: false] Extra pontuation for RULA (if task is repetitive), see referenced paper for further details  
        
        % Extra arguments [ Kine ]  
        options.kine        % kine [DQ_kinematics]: 
                            %  Passing the DQ_Kinematics of the human upper limb allow the function to compute
                            %  (1) the shoulderBase; (2) elbowPos; and (3) wristPos which in turn allow the function to compute 
                            %  (a) if shoulderRaised (with shoulderPos); (b) if handCrossLine or handOutSideBody          
        
        % Extra arguments [ verbose ]                                                                    
        options.verbose  logical = false;  % Verbose [bool]: print outputs (particularly extras) 
                                                                  
    end
    RULA = 0;
    stringpring = strcat([10,'-------------- Running fct_get_RULA in verbose mode ---------- ',10]);
    obj.printv(obj,stringpring)         
%     
% 
    %% Compute (1) the shoulderBase; (2) elbowPos; and (3) wristPos
    theta8 = obj.rhuman.kineconfig.joints7to8(joints);
    options.shoulderBase = obj.rhuman.kine.base.translation.q(2:4); 
    options.elbowPos     = obj.rhuman.kine.fkm( theta8, 4 ).translation.q(2:4); 
    options.wristPos     = obj.rhuman.getFKM(joints).translation.q(2:4);             

    
    
    %% If ('kine',DQ_kinematics) is provided, compute (1) the shoulderBase; (2) elbowPos; and (3) wristPos
    
    % Field exists (variable was provided) 
    if isfield(options,'kine')
        obj.printv(obj,'*** Computing shoulderBase, elbowPos, wristPos from dq_kinematics and joint inputs')       
        options.shoulderBase = options.kine.base.translation.q(2:4); 
        options.elbowPos     = options.kine.fkm( theta8, 4 ).translation.q(2:4); 
        options.wristPos     = options.kine.fkm( theta8 ).translation.q(2:4);         
        
        obj.printv(obj,strcat([9,'*** ShoulderBase: ',num2str(options.shoulderBase')]))
        obj.printv(obj,strcat([9,'*** elbowPos: ',num2str(options.elbowPos')]))
        obj.printv(obj,strcat([9,'*** wristPos: ',num2str(options.wristPos'),10]))
    end

    
    
    
    %% Manuallly Set Parameters
    %
    % Shoulder Raised Threshold
    shoulderRaised_eps = 0.025;  % Default 2.5 cm
    %     
    % Body Midline (distance from the right shoulder)     
    crossBodyMidline = -(obj.rhuman.kineconfig.UpperArm)/2;  % Default -15(+-) cm (half the size of upper-arm)
    %     
    % Manually Adjusted Joint-Angles  (degree)
    % Shoulder Abduction                (uncomfortable if above)
    ergoJointLimit_ShoulderAbduction = 45;  
    % Wrist Pos: Lateral bending (yaw)  (uncomfortable if above)
    ergoJointLimit_wristYaw    = [-7, 12];      
    % Wrist Twist (roll)                (uncomfortable if above)
    ergoJointLimit_wristTwist  = 70;  
    
    

    %% Compute Upper Arm Points 

    points = 0;
    deg2rad = pi/180;
           

    % -----------------------------------------------------    
    % STEP 1:  Shoulder Assessment  (Upper Arm)
    % -----------------------------------------------------    
    
    % Shoulder Elevation
    if joints(2) < 20*deg2rad        % Below 20 (best)
        points = points + 1; 
    elseif joints(2) < 45*deg2rad    % Between 20-45
        points = points + 2; 
    elseif joints(2) < 90*deg2rad    % Between 45-90
        points = points + 3; 
    else                            % Above-equal 90 (worst)
        points = points + 4; 
    end
    obj.printv(obj,strcat(['*** Shoulder Elevation:  ',num2str(points),' Points']))
                 
    
    % Shoulder Abduction (Joint limits were manually defined)
    if joints(2) > (ergoJointLimit_ShoulderAbduction)*deg2rad  && ...
       joints(1) < (ergoJointLimit_ShoulderAbduction)*deg2rad 
        points = points + 1; 
        obj.printv(obj, strcat([9,'*** Shoulder Elevation [Extra]: +1 Shoulder Abduction']) )             
        
    end

    
    % Shoulder Extra
    if options.shoulderRaised
        points = points + 1; 
        obj.printv(obj, strcat([9,'*** Shoulder Elevation [Extra]: +1 Shoulder Raised (user input)']))             
    elseif sum( options.shoulderPos )  &&   (options.shoulderPos(end) - options.shoulderBase(end)) >= shoulderRaised_eps
        points = points + 1; 
        obj.printv(obj, strcat([9,'*** Shoulder Elevation [Extra]: +1 Shoulder Raised']))             
    end                   
    if options.armSupported
        points = max(1,points - 1); 
        obj.printv(obj, strcat([9,'*** Shoulder Elevation [Extra]: -1 Arm supported (user input)']))                                
    end        
    shoulder_points = points;    
    points = 0;
           
    
    % -----------------------------------------------------    
    % STEP 2:  Elbow Assessment  (ForeArm)
    % -----------------------------------------------------    
    if joints(4) < 60*deg2rad        % Below 60 (worst)
        points = points + 2; 
    elseif joints(4) < 100*deg2rad   % Between 60 and 100 (best)
        points = points + 1; 
    else                            % Above 100 (worst)
        points = points + 2; 
    end
    obj.printv(obj, strcat(['*** Elbow (forearm):  ',num2str(points),' Points']))             
    
    % Elbow Extras 
    if options.handCrossLine || options.handOutSideBody     
        points = points + 1; 
        obj.printv(obj, strcat([9,'*** Elbow (forearm) [Extra]: +1 handCrossLine or handOutSideBody (user input)']))                                   
    elseif sum( options.shoulderPos )    &&    options.wristPos(1) < options.shoulderPos(1) + crossBodyMidline  % Crossing midline (using shoulderPos info) 
        points = points + 1; 
        obj.printv(obj, strcat([9,'*** Elbow (forearm) [Extra]: +1 Crossing midline (using shoulderPos and wristPos info) ']))                                   
    elseif sum( options.shoulderPos )==0 &&    options.wristPos(1) < options.shoulderBase(1) + crossBodyMidline % Crossing midline (using shoulderBase info because shoulderPos=0) 
        points = points + 1; 
        obj.printv(obj, strcat([9,'*** Elbow (forearm) [Extra]: +1 Crossing midline (using shoulderBase and wristPos info) ']))                                   
    elseif (options.wristPos(1) > options.elbowPos(1)) % Out of the body
        points = points + 1; 
        obj.printv(obj, strcat([9,'*** Elbow (forearm) [Extra]: +1  Out of the body (using elbowPos and wristPos info) ']))                                   
    end

    elbow_points = points;
    points = 0;    
 

    
    % -----------------------------------------------------    
    % STEP 3:  Wrist Assessment (position)
    % -----------------------------------------------------        
    if abs( joints(6) ) < 3*deg2rad      % Absolute value Below 3 (best)
        points = points + 1; 
    elseif abs(joints(6)) <  18*deg2rad  % Absolute value Below 18 
        points = points + 2; 
    else                                % Absolute value above 18 (worst)
        points = points + 3; 
    end    
    obj.printv(obj, strcat(['*** Wrist Posture:  ',num2str(points),' Points']))   
    
    % Wrist bent (Joint limits were manually defined)
    if joints(7) < (ergoJointLimit_wristYaw(1))*deg2rad || ...
       joints(7) > (ergoJointLimit_wristYaw(2))*deg2rad 
        points = points + 1; 
        obj.printv(obj, strcat([9,'*** Wrist Posture [Extra]: +1 Wrist bent sideways (yaw angle)']))                                   
    end    
        
    wristPos_points = points;        
    points = 0;   
    
    % -----------------------------------------------------    
    % STEP 4:  Wrist Assessment (twist)
    % -----------------------------------------------------            
    if abs( joints(5) ) < (ergoJointLimit_wristTwist)*deg2rad   % Below 70 (best): Manually Set!!!
        points = points + 1; 
    else                            % Above 70 or below -70 (worst)
        points = points + 2; 
    end       
    wristTwist_points = points;        
    points = 0;   
    
    
    
    
    

    % -----------------------------------------------------    
    % STEP 5:  Lookup Table Score (Upper-Limb)
    % -----------------------------------------------------               
    RULA = RULAlookupScore(shoulder_points, elbow_points, wristPos_points, wristTwist_points);
    
    RULA_EXTRA = 0;
    if options.repetitive,  RULA_EXTRA = RULA_EXTRA +1;  end 
    if options.heldstatic,  RULA_EXTRA = RULA_EXTRA +1;  end 
    if options.abrupt,      RULA_EXTRA = RULA_EXTRA +1;  end 
    if options.heavyload,   RULA_EXTRA = RULA_EXTRA +1;  end     
    RULA = RULA + RULA_EXTRA;
    
    obj.printv(obj, strcat([10,'*** Shoulder Elevation (upper arm) [Total Points]: ',num2str(shoulder_points)]))                                   
    obj.printv(obj, strcat(['*** Elbow (forearm) [Total Points]: ',num2str(elbow_points)]))                                   
    obj.printv(obj, strcat(['*** Wrist Posture [Total Points]: ',num2str(wristPos_points)]))                                   
    obj.printv(obj, strcat(['*** Wrist Twist [Total Points]: ',num2str(wristTwist_points),10]))                                   
    

    obj.printv(obj, strcat(['********* Final RULA points: ',num2str(RULA),'  **********',10]))                                   
    
    
end








%% LOOKUP TABLE
function score = RULAlookupScore(sh_pts, elb_pts, wrstPos_pts, wristTwist_pts)
    score = 0;
    
    %======================================================================
    if sh_pts==1
        if elb_pts==1
                if     wrstPos_pts==1 
                            if wristTwist_pts ==1,  score = 1;
                            else,                   score = 2;
                            end
                elseif wrstPos_pts==2,              score = 2;
                elseif wrstPos_pts==3
                            if wristTwist_pts ==1,  score = 2;
                            else,                   score = 3;
                            end
                else,                               score = 3;
                end
        elseif elb_pts==2
                if     wrstPos_pts==1,              score = 2;
                elseif wrstPos_pts==2,              score = 2;
                elseif wrstPos_pts==3,              score = 3;
                else,                               score = 3;
                end
        else
                if     wrstPos_pts==1 
                            if wristTwist_pts ==1,  score = 2;
                            else,                   score = 3;
                            end
                elseif wrstPos_pts==2,              score = 3;
                elseif wrstPos_pts==3,              score = 3;
                else,                               score = 4;
                end
        end
    %======================================================================  
    elseif sh_pts==2
        if elb_pts==1
                if     wrstPos_pts==1 
                            if wristTwist_pts ==1,  score = 2;
                            else,                   score = 3;
                            end
                elseif wrstPos_pts==2,              score = 3;
                elseif wrstPos_pts==3
                            if wristTwist_pts ==1,  score = 3;
                            else,                   score = 4;
                            end
                else,                               score = 4;
                end
        elseif elb_pts==2
                if     wrstPos_pts==1,              score = 3;
                elseif wrstPos_pts==2,              score = 3;
                elseif wrstPos_pts==3
                            if wristTwist_pts ==1,  score = 3;
                            else,                   score = 4;
                            end
                else,                               score = 4;
                end
        else
                if     wrstPos_pts==1 
                            if wristTwist_pts ==1,  score = 3;
                            else,                   score = 4;
                            end
                elseif wrstPos_pts==2,              score = 4;
                elseif wrstPos_pts==3,              score = 4;
                else,                               score = 5;
                end
        end
    %======================================================================         
    elseif sh_pts==3
        if elb_pts==1
                if     wrstPos_pts==1,              score = 3;
                elseif wrstPos_pts==2,              score = 4;
                elseif wrstPos_pts==3,              score = 4;
                else,                               score = 5;
                end
        elseif elb_pts==2
                if     wrstPos_pts==1 
                            if wristTwist_pts ==1,  score = 3;
                            else,                   score = 4;
                            end
                elseif wrstPos_pts==2,              score = 4;
                elseif wrstPos_pts==3,              score = 4;
                else,                               score = 5;
                end
        else
                if     wrstPos_pts==1,              score = 4;
                elseif wrstPos_pts==2,              score = 4;
                elseif wrstPos_pts==3
                            if wristTwist_pts ==1,  score = 4;
                            else,                   score = 5;
                            end
                else,                               score = 5;
                end
        end
    %======================================================================    
    elseif sh_pts==4
        if elb_pts==1
                if     wrstPos_pts==1,              score = 4;
                elseif wrstPos_pts==2,              score = 4;
                elseif wrstPos_pts==3
                            if wristTwist_pts ==1,  score = 4;
                            else,                   score = 5;
                            end
                else,                               score = 5;
                end
        elseif elb_pts==2
                if     wrstPos_pts==1,              score = 4;
                elseif wrstPos_pts==2,              score = 4;
                elseif wrstPos_pts==3
                            if wristTwist_pts ==1,  score = 4;
                            else,                   score = 5;
                            end
                else,                               score = 5;
                end
        else
                if     wrstPos_pts==1,              score = 4;
                elseif wrstPos_pts==2
                            if wristTwist_pts ==1,  score = 4;
                            else,                   score = 5;
                            end
                elseif wrstPos_pts==3,              score = 5;
                else,                               score = 6;
                end
        end    
    %======================================================================         
    elseif sh_pts==5
        if elb_pts==1
                if     wrstPos_pts==1,              score = 5;
                elseif wrstPos_pts==2,              score = 5;
                elseif wrstPos_pts==3
                            if wristTwist_pts ==1,  score = 5;
                            else,                   score = 6;
                            end
                else          
                            if wristTwist_pts ==1,  score = 6;
                            else,                   score = 7;
                            end                    
                end
        elseif elb_pts==2
                if     wrstPos_pts==1 
                            if wristTwist_pts ==1,  score = 5;
                            else,                   score = 6;
                            end
                elseif wrstPos_pts==2,              score = 6;
                elseif wrstPos_pts==3
                            if wristTwist_pts ==1,  score = 6;
                            else,                   score = 7;
                            end
                else,                               score = 7;
                end
        else
                if     wrstPos_pts==1,              score = 6;
                elseif wrstPos_pts==2              
                            if wristTwist_pts ==1,  score = 6;
                            else,                   score = 7;
                            end                    
                elseif wrstPos_pts==3,              score = 7;
                else          
                            if wristTwist_pts ==1,  score = 7;
                            else,                   score = 8;
                            end    
                end
        end
    %======================================================================   
    else 
        if elb_pts==1
                if     wrstPos_pts==1,              score = 7;
                elseif wrstPos_pts==2,              score = 7;
                elseif wrstPos_pts==3
                            if wristTwist_pts ==1,  score = 7;
                            else,                   score = 8;
                            end
                else          
                            if wristTwist_pts ==1,  score = 8;
                            else,                   score = 9;
                            end                    
                end
        elseif elb_pts==2
                if     wrstPos_pts==1,              score = 8;
                elseif wrstPos_pts==2,              score = 8;
                elseif wrstPos_pts==3
                            if wristTwist_pts ==1,  score = 8;
                            else,                   score = 9;
                            end
                else,                               score = 9;
                end
        else,                                       score = 9;        
        end
    %======================================================================     
    end
end



% %% function getJoints8
% function theta8 = getJoint8(thetain)
%         theta7 = thetain;    
%         theta8 = [theta7(1);  theta7(2);  theta7(1);  -theta7(3);    theta7(4); theta7(5); theta7(6); theta7(7);  ];
% end
% 
% 
% 
% 
% %% Custom validator functions
% function mustBeA(input,className)
%     % Test for specific class    
%     if ~isa(input,className)
%         error([10, strcat('*** Input must be of class: ',9,className)])
%     end
% end    

