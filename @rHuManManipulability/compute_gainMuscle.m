%% returns gain for comfortability (ranging from [1]: only muscle to [0] only ergonomics)
% 
% 
%--------------------------------------------------------------------------
%  This method is part of the class rHuManModel
%  -
% Compute gains for computing comfortability. 
% Returns gain for muscle (assuming gain for ergonomics is convex (1-gain_muscle)
% Ranges from [1]: only muscle to [0] only ergonomics
% 
%-------------------------------------------------------------------------- 
% Function inputs:
%       Speed      -   Ranges between [0,1] (for very slow to very fast task executions)
%       Intensity  -   Intensity ranging between expected minimum and maximum task-space forces [0,1]
%       Repetitive -   Repetitive has 3 modes: 0 (unique task, 0.5: repetes from time to time, 1 very repetitive (more than 4x per minute).   
% 
% Function outputs:
%       gain_Muscle  -  Gain for muscle (assuming gain for ergonomics is convex (1-gain_muscle)
%                       Ranges from [1]: only muscle to [0] only ergonomics
%-------------------------------------------------------------------------- 
% 
%  gain_Muscle = compute_costs(0.5, 0.75, 0)          
%  gain_Muscle = compute_costs(0.65, 0.25, 1)          
% 
%-------------------------------------------------------------------------- 



%% Get Forward Kinematics
% 
function gain_Muscle = compute_gainMuscle(Speed,Intensity,Repetitive)      
    gain_Muscle = (1/3)*( min(1,abs(Speed)) + min(1,abs(Intensity)) + (1-min(1,abs(Repetitive))) );
end



