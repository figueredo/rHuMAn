# RHuMAn

The rHuMAn (Rapid Human-Manipulability Assessment) tool provides
* a human kinematics model according to biomechanics literature;
* tools to analyze upper-limb muscular activity from task-space forces and accelerations; 
* assessment of ergonomics according to postural rapid techniques (e.g., RULA); 
* a dataset with biomechanics parameters for the upper-limb (e.g., muscle-length, muscle maximum isometric force, moment-arm between muscles and joints); 
* a framework to integrate ergonomics and muscular-based metrics into manipulability distributions.
* visualization assessment. 

Github page: https://gitlab.com/figueredo/rHuMAn

-----------------------------------------------------

To start using download a database of OpenSim based biomechanics assessment. 
Avaialable at: 
git clone -b OpenSimData git@gitlab.com:figueredo/rHuMAn.git

Additional requirement: dqRobotics toolbox https://dqrobotics.github.io/

-----------------------------------------------------

Other options is to use an already built datastruct: 

exdatastruct.mat



