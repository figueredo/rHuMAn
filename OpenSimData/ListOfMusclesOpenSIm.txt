DELT1
DELT2
DELT3
SUPSP
INFSP
SUBSC
TMIN
TMAJ
PECM1
PECM2
PECM3
LAT1
LAT2
LAT3
CORB
TRIlong
TRIlat
TRImed
ANC
SUP
BIClong
BICshort
BRA
BRD
ECRL
ECRB
ECU
FCR
FCU
PL
PT
PQ
FDSL
FDSR
FDSM
FDSI
FDPL
FDPR
FDPM
FDPI
EDCL
EDCR
EDCM
EDCI
EDM
EIP
EPL
EPB
FPL
APL








NAME OF MUSCLES

(SHOULDER) - Deltoid Anterior
Deltoid Middle
Deltoid Posterior
Supraspinatus
Infraspinaturs
Subscapularis
Teres Minor
Teres Mjaor
Pectoralist Major: Clavicular
Pectoralist Major: Sternal
Pectoralist Major: Ribs
Latissimus Dorsi: Thoracic
Latissimus Dorsi: Lumbar
Latissimus Dorsi: Iliac
Coracobrachialis
(ELBOW) - Triceps Long
Triceps Lateral
Triceps Medial
ANconeus
SUpinator
Biceps Long
Biceps SHort
Brachialis
Brachioradialis
(MAJOR WRIST OR FOREARM) Extensor Carpi Radialis Longus
Extensor Carpi Radialis Brevis
Extensor Carpi Ulnaris
Flexor Carpi Radialis
Flexor Carpi Ulnaris
Palmaris Longus
Pronator teres
Pronator quadratus
(WRIST/HAND MUSCLES) Flexor digitorum superficialis (digit 5)
Flexor digitorum superficialis (digit 4)
Flexor digitorum superficialis (digit 3)
Flexor digitorum superficialis (digit 2)
Flexor digitorum profundus (digit 5)
Flexor digitorum profundus (digit 4)
Flexor digitorum profundus (digit 3)
Flexor digitorum profundus (digit 2)
Extensor digitorum Communis (digit 5)
Extensor digitorum Communis (digit 4)
Extensor digitorum Communis (digit 3)
Extensor digitorum Communis (digit 2)
Extensor digiti minimi 
Extensor indicis propius
Extensor Pollicis Longus
Extensor Pollicis Brevis
Flexor Pollicis Longus
Abductor Pollicis Longus
