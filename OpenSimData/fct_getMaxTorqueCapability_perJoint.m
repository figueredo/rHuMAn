%% OpenSim: Kine and biomechanics
%--------------------------------------------------------------------------
%  This example is part of the manuscript entitled:
% 
%  xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
% 
%  Submitted to the xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
%  Date:    February, 2019
% 
%  Authors: Luis F C Figueredo
%           Lipeng Chen
%           Mehmet Dogar
%           
%  Contact information: figueredo@ieee.org
%-------------------------------------------------------------------------- 
% 
% This function takes a joint configuration and outputs the maximum torque
% generation capabilities at that point
% 
% Function inputs:
%       jointConfiguration -  Vector R7 (theoretically between 
                % humanjoints_min = [-90  0    -90  0    -90   0    -70]'*(pi/180);  
                % humanjoints_max = [130  180  +20  130  +90   +25  +70]'*(pi/180);  
                % range divided between = [9 9 7 9 7 4 7];
                % range divided between = [24.4  20  15.71   14.44    25.71  6.25  20] degrees;                
%                 
%       muscleMaxForce - Struct containing all discretized values for MaxTorque          
%                 
% 
% 
% Function output:
%       MaxTorque - Max. Muscle Torque Generation Capability per joint. 
%                   i.e., MaxTorque = (momentArm * MaxMuscleForce)
%                   If you have MaxTorque*Activation = Torque
% 
%       MuscleDescription - Output the 50 muscles (order of the vector)
% 
%-------------------------------------------------------------------------- 
% 
%-------------------------------------------------------------------------- 




%% Function load (outputs) kinematics 
% 
function [MaxTorque, MuscleDescription] = fct_getMaxTorqueCapability_perJoint(joint_input, muscleMaxForce)

FLAG_WARNING = false; 

% JOINT LIMITS
humanData.joint.limits_lower = [-90  0    -90  0    -90   0    -70]'*(pi/180);  
humanData.joint.limits_upper = [130  180  +20  130  +90   +25  +70]'*(pi/180);  
checkJointLim = fctCheckJointLimits(joint_input, 0);

if logical( sum( checkJointLim ) ) && FLAG_WARNING
    disp('[Warning]: Joint_input is above joint limits from opensim. Taking max_torque from the closest limit. ')
end





%% BIOMECHANICS
% -------------------------

biomechanics.discreteJointsPoints = getjointDiscretePoints(muscleMaxForce.joint_index_Configuration);
biomechanics.discreteSetSize(1) = length(biomechanics.discreteJointsPoints{1});
biomechanics.discreteSetSize(2) = length(biomechanics.discreteJointsPoints{2});
biomechanics.discreteSetSize(3) = length(biomechanics.discreteJointsPoints{3});
biomechanics.discreteSetSize(4) = length(biomechanics.discreteJointsPoints{4});
biomechanics.discreteSetSize(5) = length(biomechanics.discreteJointsPoints{5});
biomechanics.discreteSetSize(6) = length(biomechanics.discreteJointsPoints{6});
biomechanics.discreteSetSize(7) = length(biomechanics.discreteJointsPoints{7});



MaxTorque = getMomentArmXforceMatrix(joint_input);
%================[ DESCRIPTION OF THE 50 MUSCLES ]

MuscleDescription = [ ...                  
    {'DELT1'}, {'DELT2'}, {'DELT3'}, {'SUPSP'}, {'INFSP'}, {'SUBSC'}, {'TMIN'}, {'TMAJ'}, {'PECM1'}, {'PECM2'}, {'PECM3'}, {'LAT1'}, {'LAT2'}, {'LAT3'}, {'CORB'}, ...   % SHOULDER MUSCLES
    {'TRIlong'}, {'TRIlat'}, {'TRImed'}, {'ANC'}, {'SUP'}, {'BIClong'}, {'BICshort'}, {'BRA'}, {'BRD'}, ...  % ELBOW MUSCLES
    {'ECRL'}, {'ECRB'}, {'ECU'}, {'FCR'}, {'FCU'}, {'PL'}, {'PT'}, {'PQ'}, ... % MAJOR WRIST OR FOREARM MUSCLES
    {'FDSL'}, {'FDSR'}, {'FDSM'}, {'FDSI'}, {'FDPL'}, {'FDPR'}, {'FDPM'}, {'FDPI'}, {'EDCL'}, {'EDCR'}, {'EDCM'}, {'EDCI'}, {'EDM'}, {'EIP'}, {'EPL'}, {'EPB'}, {'FPL'}, {'APL'}]; % WRIST/HAND MUSCLES


% =================================================== [ Alphabetic order  ]
% MuscleDescription = [ 'ANC', 'APL', 'BIClong', 'BICshort', 'BRA', 'BRD', 'CORB', 'DELT1', 'DELT2', 'DELT3', ...
%                       'ECRB', 'ECRL', 'ECU', 'EDCI', 'EDCL', 'EDCM', 'EDCR', 'EDM', 'EIP', 'EPB', 'EPL', 'FCR', ... 
%                       'FCU', 'FDPI', 'FDPL', 'FDPM', 'FDPR', 'FDSI', 'FDSL', 'FDSM', 'FDSR', 'FPL', 'INFSP', ...
%                       'LAT1', 'LAT2', 'LAT3', 'PECM1', 'PECM2', 'PECM3', 'PL', 'PQ		', 'PT	', 'SUBSC', 'SUP', ...
%                       'SUPSP', 'TMAJ', 'TMIN', 'TRIlat', 'TRIlong', 'TRImed'];

              
                  
                  

%% BIOMECHANICS FUNCTIONS


% % CLASS METHOD: opSim.get_muscleForce
% % -------------------------------------------------------------------------
% %        Depending of the discretization scheme (see: getjointDiscretePoints(jointList))
% %        returns the vector (1 x 50) of Max Muscle Forces (joint-dependent)
% %      INPUT:  theta (7-joints)
% %      OUTPUT:  Max Muscle Forces (1 x 50) 
% % -------------------------------------------------------------------------
% function mforce = getMuscleForceVector(theta)
%     
%     % Get index data for the give joint
%     [indexMin, percentageIndex, indexRound] = getVectorIndexInternal(theta);
%     
%     % Initialize for speed
%     mforceTmp = zeros(7,50);
% 
%     % From the rounded-valued joints (indexes), fix all except "it" and get
%     % the linear approximation for the joint "it" muscular force
%     for it=1:7
%         lookIndexMin = indexRound;
%         lookIndexMax = indexRound;
%         lookIndexMin(it) = indexMin(it); 
%         lookIndexMax(it) =  min(opSim.biomechanics.discreteSetSize(it), (indexMin(it)+1)); 
%                
%         minMuscleEffort = opSim.biomechanics.muscleForce(  getFullArrayNumber(lookIndexMin) ,:  );
%         maxMuscleEffort = opSim.biomechanics.muscleForce(  getFullArrayNumber(lookIndexMax) ,: );
%                 
%         mforceTmp(it,:) = minMuscleEffort + (maxMuscleEffort-minMuscleEffort).*percentageIndex(it);                
%     end    
%     mforce = mean(mforceTmp);
% end




% CLASS METHOD: opSim.get_momentArm
% -------------------------------------------------------------------------
%        Depending of the discretization scheme (see: getjointDiscretePoints(jointList))
%        returns the matrix (50 x 7) of Moment Arms * Forces  (pair muscle-joint)
%      INPUT:  theta (7-joints)
%      OUTPUT: Matrix (50 x 7) of Moment Arms * Forces (final output)
% -------------------------------------------------------------------------
function marm = getMomentArmXforceMatrix(theta)
    
    % Get index data for the give joint
    [indexMin, percentageIndex, indexRound] = getVectorIndexInternal(theta);
    
    % Initialize for speed
    getIndexNumber    = getFullArrayNumber(indexRound);
    
    marm(1,:) = muscleMaxForce.joint1( getIndexNumber ,:);   
    marm(2,:) = muscleMaxForce.joint2( getIndexNumber ,:);
    marm(3,:) = muscleMaxForce.joint3( getIndexNumber ,:);
    marm(4,:) = muscleMaxForce.joint4( getIndexNumber ,:);
    marm(5,:) = muscleMaxForce.joint5( getIndexNumber ,:);
    marm(6,:) = muscleMaxForce.joint6( getIndexNumber ,:);
    marm(7,:) = muscleMaxForce.joint7( getIndexNumber ,:);
    
    % From the rounded-valued joints, fix all joints except "it" and get
    % the linear approximation for the joint "it" muscular force
    for it=1:7
        lookIndexMin = indexRound;
        lookIndexMax = indexRound;                
        lookIndexMin(it) = indexMin(it); 
        lookIndexMax(it) =  min(biomechanics.discreteSetSize(it), (indexMin(it)+1)); 
        
        getindexMinNumber(it) = getFullArrayNumber(lookIndexMin);
        getindexMaxNumber(it) = getFullArrayNumber(lookIndexMax);
    end
    minArmMoment(1,:) = muscleMaxForce.joint1( getindexMinNumber(1) ,:);    
    minArmMoment(2,:) = muscleMaxForce.joint2( getindexMinNumber(2) ,:);
    minArmMoment(3,:) = muscleMaxForce.joint3( getindexMinNumber(3) ,:);
    minArmMoment(4,:) = muscleMaxForce.joint4( getindexMinNumber(4) ,:);
    minArmMoment(5,:) = muscleMaxForce.joint5( getindexMinNumber(5) ,:);
    minArmMoment(6,:) = muscleMaxForce.joint6( getindexMinNumber(6) ,:);
    minArmMoment(7,:) = muscleMaxForce.joint7( getindexMinNumber(7) ,:);    

    maxArmMoment(1,:) = muscleMaxForce.joint1( getindexMaxNumber(1) ,:);    
    maxArmMoment(2,:) = muscleMaxForce.joint2( getindexMaxNumber(2) ,:);
    maxArmMoment(3,:) = muscleMaxForce.joint3( getindexMaxNumber(3) ,:);
    maxArmMoment(4,:) = muscleMaxForce.joint4( getindexMaxNumber(4) ,:);
    maxArmMoment(5,:) = muscleMaxForce.joint5( getindexMaxNumber(5) ,:);
    maxArmMoment(6,:) = muscleMaxForce.joint6( getindexMaxNumber(6) ,:);
    maxArmMoment(7,:) = muscleMaxForce.joint7( getindexMaxNumber(7) ,:);        

    for it=1:7
        marm(it,:) = minArmMoment(it,:) + (maxArmMoment(it,:)-minArmMoment(it,:)).*percentageIndex(it);                
    end   
       
end






% CLASS METHOD: opSim.get_jointIndexes
% -------------------------------------------------------------------------
%        Depending of the discretization scheme (see: getjointDiscretePoints(jointList))
%        returns the vector-7-index corresponding to the theta
%      INPUT:  theta (7-joints)
%      OUTPUT: vector-7 (of indexes for each joint): 
%                   1) Minimum index (floor kind of index)
%                   2) percentageIndex (percentage between next index and floor)
%                   2) indexRound (floor or ceil index, which is closer)
% -------------------------------------------------------------------------
% Get indexes for the discretization 
function [indexMin, percentageIndex, indexRound] = getVectorIndexInternal(theta7)
    
    indexMin=ones(7,1);
    percentageIndex=zeros(7,1);
    for itloop=1:7
        indexMin(itloop) = max(1,sum( biomechanics.discreteJointsPoints{itloop} <= theta7(itloop)  )); 
        if indexMin(itloop)==biomechanics.discreteSetSize(itloop) 
            percentageIndex(itloop)=0;
            indexRound(itloop) = indexMin(itloop);
        else
            stepMin  = biomechanics.discreteJointsPoints{itloop}(indexMin(itloop));
            stepNext = biomechanics.discreteJointsPoints{itloop}(indexMin(itloop)+1);
            percentageIndex(itloop) = ( theta7(itloop)-stepMin )/(stepNext-stepMin); 
            
            indexRound(itloop) = indexMin(itloop);
            if percentageIndex(itloop)>0.5
                indexRound(itloop) = indexMin(itloop) + 1;
            end            
        end
        
    end
    
    
end




% CLASS METHOD:  opSim.biomechanics.getListNumber
% -------------------------------------------------------------------------
%        Locate a given index within the full data package (usually 94500, 
%        but depends on the size of the getjointDiscretePoints(jointList);
%      INPUT:  vector-7 (of indexes for each joint) 
%      OUTPUT: Line number corresponding to the index
% -------------------------------------------------------------------------
function vectorIndex = getFullArrayNumber(indexVec)
    
%     indexVec = round( (thetax- humanjoints_min  )./jointDiscretizationInterv);    
%     percentageIndex = mod( thetax - humanjoints_min, jointDiscretizationInterv)./jointDiscretizationInterv;
%     indexVec = indexVec  + double( percentageIndex==0 );       
    mult(1) = biomechanics.discreteSetSize(7);
    mult(2) = mult(1)*biomechanics.discreteSetSize(6);
    mult(3) = mult(2)*biomechanics.discreteSetSize(5);
    mult(4) = mult(3)*biomechanics.discreteSetSize(4);
    mult(5) = mult(4)*biomechanics.discreteSetSize(3);
    mult(6) = mult(5)*biomechanics.discreteSetSize(2);
    
    vectorIndex = indexVec(7); 
    vectorIndex = vectorIndex + mult(1)*(indexVec(6)-1); 
    vectorIndex = vectorIndex + mult(2)*(indexVec(5)-1); 
    vectorIndex = vectorIndex + mult(3)*(indexVec(4)-1);
    vectorIndex = vectorIndex + mult(4)*(indexVec(3)-1);
    vectorIndex = vectorIndex + mult(5)*(indexVec(2)-1);
    vectorIndex = vectorIndex + mult(6)*(indexVec(1)-1);
end


% INTERNAL METHOD:  
% -------------------------------------------------------------------------
%      Return 7-cell with the discretized values for each joint
% -------------------------------------------------------------------------
function jointDiscretePoints = getjointDiscretePoints(jointList)
    multipl=1;
    for itloop=7:-1:2    
        % Get the range index per joint 
        % Theoretically: [24.4  20  15.71   14.44    25.71  6.25  20] degrees         
        equalIndex = ( find(jointList(:,itloop)==jointList(1,itloop),multipl+1) );
        % Associate corresponding index to joint
        jointDiscretePoints{itloop} = jointList(1:multipl:equalIndex(end)-1,itloop);    
        multipl = equalIndex(end)-1;
    end
    jointDiscretePoints{1} = jointList(1:multipl:end,1);    
end




%% Check Joint Limits
function [boolJointLimit, lowerLimit, upperLimit] = fctCheckJointLimits(theta_cur,offset)
    
    lowerlim = humanData.joint.limits_lower;
    upperlim = humanData.joint.limits_upper;
    if exist('offset','var') 
        lowerlim = lowerlim -offset*abs(humanData.joint.limits_lower);
        upperlim = upperlim +offset*abs(humanData.joint.limits_upper);
    end
    lowerLimit = theta_cur < lowerlim;
    upperLimit = theta_cur > upperlim;
    boolJointLimit = sum(double(lowerLimit)+  double(upperLimit))>0;
end

end