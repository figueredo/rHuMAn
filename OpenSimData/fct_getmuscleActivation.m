%% OpenSim: Kine and biomechanics
%--------------------------------------------------------------------------
%  This example is part of the manuscript entitled:
% 
%  xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
% 
%  Submitted to the xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
%  Date:    February, 2019
% 
%  Authors: Luis F C Figueredo
%           Lipeng Chen
%           Mehmet Dogar
%           
%  Contact information: figueredo@ieee.org
%-------------------------------------------------------------------------- 
% 
% This function takes a joint configuration and outputs the maximum torque
% generation capabilities at that point
% 
% Function inputs:
% 
%       Torque_input - Input Torque Applied to the human joints                
%                 
%       maxTorqueValuesperMuscle - Max. Muscle Torque Generation Capability per joint. 
%                   i.e., MaxTorque = (momentArm * MaxMuscleForce)
%                   If you have MaxTorque*Activation = Torque
% 
% 
% Function output:
%       MuscleActivation - MuscleActivation Vector for N muscles [0,1]
%                   Obtained from static optimization
% 
%-------------------------------------------------------------------------- 
% 
%-------------------------------------------------------------------------- 




%% Function load (outputs) kinematics 
% 
function [MuscleActivation, MuscleActivationNORM] = fct_getmuscleActivation(torque_input, maxTorqueValuesperMuscle)




% CLASS METHOD: opSim.staticOptimization
% -------------------------------------------------------------------------
%      Returns the 50-muscle (1 x 50) activation between [0, 1]
                % Computing muscular activity            
                % Optimization:
                %---------------------------------------------
                % min(|| C * x   - d ||^2)       (x=alpha) 
                %      subject-to  A * x <=  b   (not-applicable)
                %                  Aeq * x = beq (Aeq=arm.*maxForce  |  Beq=torque)
                %---------------------------------------------
%      INPUT:  theta (7-joints)
%      OUTPUT:  Muscle activation level (1 x 50) 
% -------------------------------------------------------------------------
           
        num_muscles = length(maxTorqueValuesperMuscle);
        
        % Constant parameters for optimization
        linparam.A = zeros(num_muscles);
        linparam.b = zeros(num_muscles,1);    
        lb = zeros(num_muscles,1);    
        ub = ones(num_muscles,1);         
%         lb = -ub;
        
        % Static Optimization constraint wrt torque    
        %Aeqtemp = maxTorqueValuesperMuscle;

        if sum(abs(maxTorqueValuesperMuscle(1,:)))<0.01
%             disp('Outputing')
            linparam.Aeq = maxTorqueValuesperMuscle(2:end,:);
            torquesum = sum(torque_input(2:3));
            torque_input(2:3) = (torque_input(2:3)./torquesum).*torque_input(1);
            linparam.beq = torque_input(2:end); 
            %disp('removing joint elv-angle')
        else
            linparam.Aeq = maxTorqueValuesperMuscle;
            linparam.beq = torque_input(1:end); 
        end                                
        options = optimoptions('lsqlin','Algorithm','interior-point','Display','off');        
%                                 options = optimoptions('lsqlin','Algorithm','interior-point');

        % alpha = lsqlin(eye(sim.numMuscles),zeros(sim.numMuscles,1), linparam.A, linparam.b , linparam.Aeq, linparam.beq, lb, ub  );
        MuscleActivation = lsqlin(eye(num_muscles),zeros(num_muscles,1), linparam.A, linparam.b , linparam.Aeq, linparam.beq, lb, ub, [] , options );
%                                 alpha = lsqlin(eye(num_muscles),zeros(num_muscles,1), linparam.Aeq, linparam.beq , linparam.A, linparam.b, lb, ub, [] , options );

        if isempty(MuscleActivation)
            %disp('ERROR: MUSCLE ACTIVATION RETURNED EMPTY')
            MuscleActivation = ones(50,1);
        end        
        MuscleActivationNORM = sqrt( MuscleActivation'*MuscleActivation );  
        


end

 